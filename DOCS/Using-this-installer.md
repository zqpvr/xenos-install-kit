# Optimal System Specs
PC Type: Laptop preferred <br/>
BIOS Type: UEFI (EFI) <br/>
CPU Type: x86_64 <br/>
Drive Type: Single preferred (HDD, SSD or NVMe) <br/>
GPU Type: Integrated preferred (Non-optimus) <br/>
TPM Type: 2.0 supported

## Usage
Before using the Xenos Install Kit please first attempt to install Arch Linux with their [installation guide](https://wiki.archlinux.org/index.php/Installation_Guide) if you haven't already.

### Preface
A list of all global variables, their purpose and their accepted values.

#### Legend
 | Character | Meaning |
 | --- | --- |
 | ! | Requires user input |
 | x | Doesn't require user input |
 | ~ | Placeholder element, usually means read the Arch wiki |

#### S1.sh
 | ! or x | Name | Purpose | Accepted values |
 | --- | --- | --- | --- |
 | ! | keymap_language_split | The keyboard keymap and language combined | ~ |
 | x | keymap | The keyboard keymap | ~ |
 | ! | drive_name | The drive name to install to | ~ |
 | ! | luks_password | The luks passphrase | ~ |
 | x | luks_container_name | The luks container name | ~ |
 | x | luks_volume_name | The luks volume name | ~ |
 | ! | luks_volume_fs_type | The luks volume filesystem type | btrfs, ext4 or xfs |
 | x | kernel_type | The kernel type | ~ |
 | x | mirrorlist_url | The url of the desired mirrorlist | ~ |
 | x | core_pack | The core packages to be installed | ~ |
 | x | is_intel_cpu | CPU detection variable | ~ |
 | x | cpu_type | The CPU type | ~ |
 | ! | systemd_git_url | The url to systemd-git compiled by S3 | [Host 1](https://oshi.at/) [Host 2](https://transfer.sh/) [Host 3](https://temp.sh/) |
 | ! | dns_servers | The dns servers used for dhcpcd | ~ |
 | ! | timezone | The timezone | ~ |
 | x | language | The language | ~ |
 | ! | hostname | The hostname | ~ |
 | ! | username | The username | ~ |
 | ! | userpass | The userpass | ~ |
 | ! | using_usb_keyboard | Provides early enablement of the keyboard and keymap hooks in initramfs | y to use, leave empty to ignore |
 | x | mkinitcpio_hooks | The hooks mkinitcpio will use | ~ |
 | x | systemdboot_entry_name | The bootloader entry name | ~ |
 | x | systemdboot_entry_content | The bootloader entry content | ~ |
 | x | systemdboot_entry_content_options | The bootloader entry content options | ~ |
 | x | luks_uuid | The luks uuid | ~ |

#### S2.sh
 | ! or x | Name | Purpose | Accepted values |
 | --- | --- | --- | --- |
 | x | cpu_cores | Specifies the total number of cpu cores | ~ |
 | x | is_a_gpu | GPU detection variable | ~ |
 | x | is_amd_gpu | AMD GPU detection variable | ~ |
 | x | is_intel_gpu | Intel GPU detection variable | ~ |
 | x | is_nvidia_gpu | Nvidia GPU detection variable | ~ |
 | ! | install_nvidia_driver | Whether or not to install the nvidia driver | y to use, leave empty to ignore |
 | ! | install_optimus_manager | Whether or not to install optimus manager | y to use, leave empty to ignore |
 | ! | install_only_essentials | When used only installs essential packages. Useful for testing purposes, debugging or for extra security | y to use, leave empty to ignore |
 | x | has_tpm | TPM detection variable | ~ |
 | x | custom_user_agent | The custom user agent to use | ~ |
 | ! | custom_web_browser | The custom web browser to install | ~ |
 | ! | luakit_searx_instance_name | Determines which searx instance luakit will use | demoniak, namejeff or zhenyapav |
 | x | luakit_searx_instance_server | The searx instance server that luakit will use | ~ |
 | x | luakit_searx_instance_server_search | The searx instance server with the search query appended that luakit will use | ~ |
 | ! | luakit_adblock_ctl | The adblock filters luakit will use | ~ |
 | ! | custom_core_pack_packages | Custom packages to install | ~ |
 | ! | custom_aur_packages | Custom aur packages to install | ~ |
 | ! | openvpn_delevation_user | The Openvpn delevation user | ~ |
 | x | openvpn_delevation_user_uid_gid | The Openvpn delevation users uid and gid | ~ |
 | x | crda_region_locale_split | The crda_region and locale combined | ~ |
 | x | crda_region | Ensures your wireless activity is in compliance with your countries regulatory domain | ~ |
 | x | umask | The umask value to use | ~ |
 | ! | machine_id | The machine-id the machine will use | whonix or xenos |
 | ! | os_release | The os-release information the machine will use | gentoo, lfs or linux |
 | x | locale | Ensures your locale is enforced | ~ |

#### S3.sh
 | ! or x | Name | Purpose | Accepted values |
 | --- | --- | --- | --- |
 | ! | install_brave_nightly_bin | Whether or not to install brave nightly bin | y to use, leave empty to ignore |
 | ! | install_luakit_git | Whether or not to install luakit git | y to use, leave empty to ignore |
 | ! | install_linux_firmware_git | Whether or not to install linux firmware git | y to use, leave empty to ignore |
 | ! | install_linux_hardened_git | Whether or not to install linux hardened git | y to use, leave empty to ignore |
 | ! | install_networkmanager_git | Whether or not to install networkmanager git | y to use, leave empty to ignore |
 | ! | install_systemd_git | Whether or not to install systemd git | y to use, leave empty to ignore |
 | ! | install_xorg_server_git | Whether or not to install xorg server git | y to use, leave empty to ignore |
 | x | is_intel_cpu | CPU detection variable | ~ |
 | x | first_use | First use detection variable | ~ |
 | x | packages_created | Successful package compilation detection variable | ~ |

### Required: Populate variables
1) Begin by [downloading](https://gitlab.com/xenos-install-kit/xenos-install-kit/-/archive/main/xenos-install-kit-main.zip) and extracting the latest release.
2) Open S1.sh in a text editor of your choice.
3) Populate its variables according to its chart found in [preface](#s1sh).
4) Open S2.sh in a text editor of your choice.
5) Populate its variables according to its chart found in [preface](#s2sh).

### Optional: Adding systemd-git
When using the Xenos Install Kit for the first time, S1.sh doesn't include systemd-git. <br/>
You will not get the full benefits of this installer without including it. <br/>
To get a proper copy of it, simply continue the installation and compile it yourself using S3.sh from advanced usage below. <br/>
When you have a copy of systemd-git:
1) Open S1.sh in a text editor of your choice.
2) Populate systemd_git_url.

### Optional: Enable optimus support
If it's imperative that you must use your dedicated GPU in an optimus enabled system then some extra legwork needs completed: <br/>
1) Open S1.sh in a text editor of your choice.
2) Comment out the following lines:
```
# systemdboot_entry_content_options="${systemdboot_entry_content_options} lockdown=confidentiality"
# systemdboot_entry_content_options="${systemdboot_entry_content_options} module.sig_enforce=1"
```
3) Open S2.sh in a text editor of your choice.
4) Add y to the variables install_nvidia_driver and
install_optimus_manager:
```
install_nvidia_driver="y"
install_optimus_manager="y"
```

### Required: General usage
1) Upload S1.sh to a host of your choice.
2) Record the url to the file.
3) Tar.xz the entire S2 folder and upload it there as-well.
```
tar -caf S2.tar.xz S2/
```
4) Record the url to the file.
5) Boot into the Arch USB.
6) Ensure you're connected to the internet.
7) Curl S1.sh and execute it:
```
curl -o S1.sh "${your_S1.sh_url}"
bash S1.sh
```
8) Stay near the system to make sure you have no issues.
9) After rebooting and logging in do the same for S2.sh:
```
curl -o S2.tar.xz "${your_S2.tar.xz_url}"
tar -xaf S2.tar.xz
cd S2
bash S2.sh
```
10) Stay near the system to make sure you have no issues.
11) After rebooting login again and change your password with passwd. This ensures password hashing is properly implemented.
12) Lastly reconnect to the internet and run xenos-setup-time.sh:
```
doas bash /usr/bin/xenos-setup-time.sh
```

### Optional: Setting up Openvpn with openvpn-update-systemd-resolved
If you intend to use Openvpn with openvpn-update-systemd-resolved then here is how to do so: <br/>
1) In each of your .ovpn files add:
```
script-security 2
up /usr/bin/update-systemd-resolved
up-restart
dhcp-option DOMAIN-ROUTE .
```
2) To use delevation add:
```
user ${openvpn_delevation_user}
group ${openvpn_delevation_user}
```
3) Next move your .ovpn files to /home/shared:
```
doas cp -R VPN-FILES/ /home/shared
doas find /home/shared -execdir chown root:root {} +
doas find /home/shared -type d -execdir chmod 755 {} +
doas find /home/shared -type f -execdir chmod 644 {} +
```
4) Finally you use Openvpn like so:
```
doas bash /usr/bin/xenos-control-dns.sh
or
doas bash /usr/bin/xenos-control-dns.sh -l -s
then
doas openvpn "/home/shared/${some_ovpn_file}"
```

### Optional: Advanced usage
After completeing S1 and S2 it's highly recommended you continue with S3: <br/>
1) Open S3.sh in a text editor of your choice.
2) Update its variables according to its chart found in [preface](#s3sh).
3) Execute it:
```
bash S3.sh
```
4) Upon a successful reboot validate everything is working and:
```
bash S3-M.sh
```
