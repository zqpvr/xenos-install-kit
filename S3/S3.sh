#!/usr/bin/bash


#################################################
#
#           _______  _        _______  _______
# |\     /|(  ____ \( (    /|(  ___  )(  ____ \
# ( \   / )| (    \/|  \  ( || (   ) || (    \/
#  \ (_) / | (__    |   \ | || |   | || (_____
#   ) _ (  |  __)   | (\ \) || |   | |(_____  )
#  / ( ) \ | (      | | \   || |   | |      ) |
# ( /   \ )| (____/\| )  \  || (___) |/\____) |
# |/     \|(_______/|/    )_)(_______)\_______)
#
#
# This file is a part of the Xenos Install Kit.
# It adheres to the GNU GPL license.
#
# https://gitlab.com/xenos-install-kit/xenos-install-kit
#
# © 2020-2024
#
#
#################################################


# Print commands before executing and exit when any command fails
set -xe


_proto_pacman(){
  local ptype=$1
  local pkgs=$2

  case $ptype in
    install)
      doas pacman -S --noconfirm $pkgs
      ;;
    install-u)
      doas pacman -U $pkgs
      ;;
    remove)
      doas pacman -Rns --noconfirm $pkgs
      ;;
  esac

  return 0
}

_proto_makepkg(){
  local pkgname=$1
  local optimized_makepkg='makepkg -C -c --skippgpcheck --nocheck'

  case $pkgname in
    linux-hardened* | networkmanager* | systemd* | xf86-video-amdgpu* | xorg-server*)
      cd "aur/${pkgname}"
      $optimized_makepkg
      cd ../../
      ;;
    *)
      cd aur
      if [[ ! -d "${pkgname}" ]]; then
        git clone "https://aur.archlinux.org/${pkgname}.git"
      fi
      cd "${pkgname}"
      rm -f .SRCINFO
      sed -i "s/^options=.*//g" PKGBUILD
      sed -i "s/install-zst/install-xz/g" PKGBUILD
      sed -i "s/.zst/.xz/g" PKGBUILD
      $optimized_makepkg
      cd ../../
      ;;
  esac

  return 0
}

_proto_movepkg(){
  local pkgdir=$1
  local pkgnames=$2

  if [[ ! -d "${pkgdir}" ]]; then
    mkdir -p "${pkgdir}"
  fi

  for pkg in $pkgnames
  do
    if [[ -f "${pkg}" && -s "${pkg}" ]]; then
      mv "${pkg}" "${pkgdir}"
    fi
  done

  return 0
}

initialize(){
  #
  ## Variable prep
  #
  install_brave_nightly_bin="y"
  install_luakit_git="y"
  install_linux_firmware_git="y"
  install_linux_hardened_git="y"
  install_networkmanager_git="y"
  install_systemd_git="y"
  install_xorg_server_git="y"
  is_intel_cpu=$(lscpu | grep -i "intel(r)" 2> /dev/null || echo "")
  first_use=$(zgrep "archlinux" /proc/config.gz 2> /dev/null || echo "")
  packages_created=""

  return 0
}

install_brave_nightly_bin(){
  local brave_nightly_bin_dep=""

  #_proto_pacman "install" "${brave_nightly_bin_dep}"
  _proto_makepkg "brave-nightly-bin"
  #_proto_pacman "remove" "${brave_nightly_bin_dep}"

  return 0
}

install_luakit_git(){
  local luakit_git_dep="lua51-filesystem luajit webkit2gtk webkit2gtk-4.1"

  #_proto_pacman "install" "${luakit_git_dep}"
  _proto_makepkg "luakit-git"
  #_proto_pacman "remove" "${luakit_git_dep}"

  return 0
}

install_intel_ucode_git(){
  local intel_ucode_git_dep="iucode-tool"

  _proto_pacman "install" "${intel_ucode_git_dep}"
  _proto_makepkg "intel-ucode-git"
  _proto_pacman "remove" "${intel_ucode_git_dep}"

  return 0
}

install_linux_firmware_git(){
  local linux_firmware_git_dep="rdfind"

  _proto_pacman "install" "${linux_firmware_git_dep}"
  _proto_makepkg "linux-firmware-git"
  _proto_pacman "remove" "${linux_firmware_git_dep}"

  return 0
}

install_linux_hardened_git(){
  local linux_hardened_git_dep="bc cpio graphviz imagemagick python-sphinx texlive-latexextra"

  doas bash "aur-pkg-scripts/linux-hardened-insert-modules.sh"
  _proto_pacman "install" "${linux_hardened_git_dep}"
  _proto_makepkg "linux-hardened-git"
  _proto_pacman "remove" "${linux_hardened_git_dep}"

  return 0
}

install_networkmanager_git(){
  local networkmanager_git_dep="glib2-docs gobject-introspection gtk-doc intltool meson perl-yaml vala"
  local networkmanager_bloat_cleanup="bluez-libs libmm-glib libteam"

  _proto_pacman "install" "${networkmanager_git_dep}"
  _proto_makepkg "networkmanager-git"
  _proto_pacman "remove" "${networkmanager_git_dep}"

  if [[ -n "${first_use}" ]]; then
    _proto_pacman "remove" "${networkmanager_bloat_cleanup}"
  fi

  return 0
}

install_systemd_git(){
  local systemd_git_dep="bpf clang docbook-xsl gperf intltool kexec-tools lib32-gcc-libs llvm meson python-jinja python-lxml python-pyelftools quota-tools"
  local systemd_bloat_cleanup="libfido2 libmicrohttpd"

  _proto_pacman "install" "${systemd_git_dep}"
  _proto_makepkg "systemd-git"
  _proto_pacman "remove" "${systemd_git_dep}"

  if [[ -n "${first_use}" ]]; then
    _proto_pacman "remove" "${systemd_bloat_cleanup}"
  fi

  return 0
}

install_xf86_video_amdgpu_git(){
  local xf86_video_amdgpu_git_dep="xorg-util-macros"

  _proto_pacman "install" "${xf86_video_amdgpu_git_dep}"
  _proto_makepkg "xf86-video-amdgpu-git"
  _proto_pacman "remove" "${xf86_video_amdgpu_git_dep}"

  return 0
}

install_xorg_server_git(){
  local xorg_server_git_dep="libxaw libxres meson xcb-util-renderutil xorg-font-util xorg-util-macros xtrans"

  _proto_pacman "install" "${xorg_server_git_dep}"
  _proto_makepkg "xorg-server-git"
  _proto_pacman "remove" "${xorg_server_git_dep}"

  return 0
}

package_check(){
  local package_verification_list=($(find aur/ -type f -iname "*.pkg.tar.xz"))

  if [[ -n "${package_verification_list[@]}" ]]; then
    packages_created="y"
  fi

  return 0
}

finalize(){
  local compiled_package_dir="COMPILED-FREQUENT"
  local compiled_package_list=($(find aur/ -type f -iname "*.pkg.tar.xz"))

  # Create the compiled_package_dir and move all compiled packages to it
  if [[ -n "${compiled_package_list[@]}" ]]; then
    _proto_movepkg "${compiled_package_dir}" "${compiled_package_list[*]}"
  fi

  # Now we need to verify which packages actually need installed
  compiled_package_list=($(ls -A "${compiled_package_dir}"))

  if [[ -n "${compiled_package_list[@]}" ]]; then
    local compiled_package_list_to_install=""

    for package in "${compiled_package_list[@]}"
    do
      local package_clean=$(echo "${package}" | sed "s/-[0-9].*//g")
      local is_package_installed=$(pacman -Qs "${package_clean}")

      if [[ -n "${is_package_installed}" ]]; then
        compiled_package_list_to_install="${compiled_package_list_to_install} ${compiled_package_dir}/${package}"
      else
        local verify_package_installation=""

        read -p "Install ${package_clean}? y/n " verify_package_installation

        if [[ -n "${verify_package_installation}" && "${verify_package_installation}" == y ]]; then
          compiled_package_list_to_install="${compiled_package_list_to_install} ${compiled_package_dir}/${package}"
        fi
      fi
    done
  fi

  # Install all packages at once
  if [[ -n "${compiled_package_list_to_install}" ]]; then
    _proto_pacman "install-u" "$compiled_package_list_to_install"
  fi

  # Reconfigure the bootloader only when using linux-hardened-git and on first use
  if [[ -n "${install_linux_hardened_git}" && -n "${first_use}" ]]; then
    local entry_name=$(doas find /boot/loader/entries/ -type f)
    doas sed -i "s/linux-hardened/linux-hardened-git/g" "${entry_name}"
  fi

  # Always regenerate systemd-boot and mkinitcpio
  doas bootctl install
  doas mkinitcpio -P

  return 0
}

exit_installer(){
  # Prompt for shutdown
  read -p "Xenos final install complete. Press [Enter] key to shutdown..."
  systemctl poweroff

  return 0
}


initialize

if [[ -n "${install_brave_nightly_bin}" ]]; then
  install_brave_nightly_bin
fi

if [[ -n "${install_luakit_git}" ]]; then
  install_luakit_git
fi

if [[ -n "${install_linux_firmware_git}" ]]; then
  if [[ -n "${is_intel_cpu}" ]]; then
    install_intel_ucode_git
  fi

  install_linux_firmware_git
fi

if [[ -n "${install_linux_hardened_git}" ]]; then
  install_linux_hardened_git
fi

if [[ -n "${install_networkmanager_git}" ]]; then
  install_networkmanager_git
fi

if [[ -n "${install_systemd_git}" ]]; then
  install_systemd_git
fi

if [[ -n "${install_xorg_server_git}" ]]; then
  if [[ -z "${is_intel_cpu}" ]]; then
    install_xf86_video_amdgpu_git
  fi

  install_xorg_server_git
fi

package_check

if [[ -n "${packages_created}" ]]; then
  finalize
  exit_installer
fi

exit 0

