#!/usr/bin/bash


#################################################
#
#           _______  _        _______  _______
# |\     /|(  ____ \( (    /|(  ___  )(  ____ \
# ( \   / )| (    \/|  \  ( || (   ) || (    \/
#  \ (_) / | (__    |   \ | || |   | || (_____
#   ) _ (  |  __)   | (\ \) || |   | |(_____  )
#  / ( ) \ | (      | | \   || |   | |      ) |
# ( /   \ )| (____/\| )  \  || (___) |/\____) |
# |/     \|(_______/|/    )_)(_______)\_______)
#
#
# This file is a part of the Xenos Install Kit.
# It adheres to the GNU GPL license.
#
# https://gitlab.com/xenos-install-kit/xenos-install-kit
#
# © 2020-2024
#
#
#################################################


purge_deprecated_networkmanager_git_dirs(){
  local networkmanager_git_version=$(pacman -Qs networkmanager-git | grep -i "local" | sed "s|local.* ||g")
  local networkmanager_git_path=/usr/lib/NetworkManager/*

  for item in $networkmanager_git_path
  do
    if [[ -d "${item}" ]]; then
      if [[ "${item##*/}" == 1* && "${item##*/}" != "${networkmanager_git_version}" ]]; then
        rm -rf "${item}"
      fi
    fi
  done

  return 0
}

purge_deprecated_systemd_git_hooks(){
  local systemd_git_hooks_list=($(pacman -Ql systemd-git | grep -i "libalpm" | grep -i "\.hook" | sed "s|.*hooks/||g"))
  local systemd_git_hooks_path=/usr/share/libalpm/hooks/*

  for item in $systemd_git_hooks_path
  do
    if [[ -f "${item}" ]]; then
      if [[ "${item##*/}" == *systemd*hook && ! "${systemd_git_hooks_list[*]}" =~ "${item##*/}" ]]; then
        rm -f "${item}"
      fi
    fi
  done

  return 0
}

purge_deprecated_systemd_git_shared_objects(){
  local systemd_git_version=$(pacman -Qs systemd-git | grep -i "local" | sed "s|local.* ||g")
  local systemd_git_shared_objects_path=/usr/lib/systemd/*

  for item in $systemd_git_shared_objects_path
  do
    if [[ -f "${item}" ]]; then
      if [[ "${item##*/}" == libsystemd*so && "${item##*/}" != *"${systemd_git_version}"*so ]]; then
        rm -f "${item}"
      fi
    fi
  done

  return 0
}


## Initialize script
if (( $EUID != 0 )); then
  exit 0
fi

purge_deprecated_networkmanager_git_dirs
purge_deprecated_systemd_git_hooks
purge_deprecated_systemd_git_shared_objects

exit 0

