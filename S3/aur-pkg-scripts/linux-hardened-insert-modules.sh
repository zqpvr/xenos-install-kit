#!/usr/bin/bash


#################################################
#
#           _______  _        _______  _______
# |\     /|(  ____ \( (    /|(  ___  )(  ____ \
# ( \   / )| (    \/|  \  ( || (   ) || (    \/
#  \ (_) / | (__    |   \ | || |   | || (_____
#   ) _ (  |  __)   | (\ \) || |   | |(_____  )
#  / ( ) \ | (      | | \   || |   | |      ) |
# ( /   \ )| (____/\| )  \  || (___) |/\____) |
# |/     \|(_______/|/    )_)(_______)\_______)
#
#
# This file is a part of the Xenos Install Kit.
# It adheres to the GNU GPL license.
#
# https://gitlab.com/xenos-install-kit/xenos-install-kit
#
# © 2020-2024
#
#
#################################################


insert_modules(){
  # This adds support for the initramfs encrypt and lvm2 hook
  # These modules aren't loaded on login by default
  # Without their inclusion the system fails to boot
  local modulectl=(
    "dm_bio_prison"
    "dm_bufio"
    "dm_cache"
    "dm_cache_smq"
    "dm_crypt"
    "dm_integrity"
    "dm_log"
    "dm_mirror"
    "dm_mod"
    "dm_region_hash"
    "dm_snapshot"
    "dm_thin_pool"
    "dm_persistent_data"
  )

  # This adds support for some usb devices
  modulectl=(
    "${modulectl[@]}"
    "uas"
    "usb_storage"
    "usbhid"
  )

  # This adds support for some usb filesystems
  modulectl=(
    "${modulectl[@]}"
    "exfat"
    "fat"
    "vfat"
  )

  # This adds support for some wireless devices
  modulectl=(
    "${modulectl[@]}"
    "ath9k_htc"
    "rtw88_8821ce"
  )

  for ctl in "${modulectl[@]}"
  do
    local module_check=$(modinfo "${ctl}" 2> /dev/null || echo "")

    if [[ -n "${module_check}" ]]; then
      modprobe -i "${ctl}"
    fi
  done

  return 0
}


## Initialize script
if (( $EUID != 0 )); then
  exit 0
fi

insert_modules

exit 0

