#!/usr/bin/bash


#################################################
#
#           _______  _        _______  _______
# |\     /|(  ____ \( (    /|(  ___  )(  ____ \
# ( \   / )| (    \/|  \  ( || (   ) || (    \/
#  \ (_) / | (__    |   \ | || |   | || (_____
#   ) _ (  |  __)   | (\ \) || |   | |(_____  )
#  / ( ) \ | (      | | \   || |   | |      ) |
# ( /   \ )| (____/\| )  \  || (___) |/\____) |
# |/     \|(_______/|/    )_)(_______)\_______)
#
#
# This file is a part of the Xenos Install Kit.
# It adheres to the GNU GPL license.
#
# https://gitlab.com/xenos-install-kit/xenos-install-kit
#
# © 2020-2024
#
#
#################################################


# Print commands before executing and exit when any command fails
set -xe


_proto_sed_bulk(){
  local sedfile=$1
  shift
  local sedctl=("$@")

  if [[ -n "${sedfile}" && -f "${sedfile}" && -s "${sedfile}" ]]; then
    for sed in "${sedctl[@]}"
    do
      if [[ "${sed:0:1}" == "^" ]]; then
        local sed_cleaned=$(echo "${sed}" | sed "s|\^||g")

        sudo sed -i "s|^#${sed_cleaned}|g" "${sedfile}"
        sudo sed -i "s|^${sed_cleaned}|g" "${sedfile}"
      else
        sudo sed -i "s|${sed}|g" "${sedfile}"
      fi
    done
  else
    echo "Warning: ${sedfile} doesn't exist. Now exiting..."
    exit 0
  fi

  return 0
}

_proto_makepkg(){
  local pkgname=$1
  local optimized_makepkg='makepkg -C -csir --skippgpcheck --nocheck --noconfirm'

  case $pkgname in
    2bwm* | openvpn-update*)
      cd "aur/${pkgname}"
      $optimized_makepkg
      cd ../../
      ;;
    *)
      cd aur
      if [[ ! -d "${pkgname}" ]]; then
        git clone "https://aur.archlinux.org/${pkgname}.git"
      fi
      cd "${pkgname}"
      rm -f .SRCINFO
      sed -i "s/^options=.*//g" PKGBUILD
      sed -i "s/install-zst/install-xz/g" PKGBUILD
      sed -i "s/.zst/.xz/g" PKGBUILD
      $optimized_makepkg
      cd ../../
      ;;
  esac

  return 0
}

_proto_purge_loop(){
  local purgectl=$1

  for ctl in $purgectl
  do
    if [[ -f "${ctl}" ]]; then
      sudo rm -f "${ctl}"
      sudo touch "${ctl}"
      sudo chmod 600 "${ctl}"
      sudo chattr +i "${ctl}"
    fi
  done

  return 0
}

initialize(){
  #
  ## Variable prep
  #
  cpu_cores=$(nproc --all)
  cpu_cores=$(("${cpu_cores}"+1))
  is_a_gpu=$(lspci | grep -i "vga\|3d")
  is_amd_gpu=$(echo "${is_a_gpu}" | grep -i "amd" 2> /dev/null || echo "")
  is_intel_gpu=$(echo "${is_a_gpu}" | grep -i "intel" 2> /dev/null || echo "")
  is_nvidia_gpu=$(echo "${is_a_gpu}" | grep -i "nvidia" 2> /dev/null || echo "")
  install_nvidia_driver=""
  install_optimus_manager=""
  install_only_essentials=""
  has_tpm=$(cat /sys/class/tpm/tpm0/tpm_version_major 2> /dev/null || echo "")
  custom_user_agent=$(curl -s "https://www.whatismybrowser.com/guides/the-latest-user-agent/safari" | grep -i "span class=\"code\"" | head -n 1 | sed "s|.*span class=\"code\">||g" | sed "s|</.*||g" | sed "s/^[ \t]*//;s/[ \t]*$//" | sed "/^$/d")
  custom_web_browser=""

  # Web Browser (1/3)
  if [[ -n "${custom_web_browser}" ]]; then
    if [[ "${custom_web_browser}" == "luakit-git" ]]; then
      luakit_searx_instance_name="demoniak"

      case $luakit_searx_instance_name in
        demoniak)
          # Switzerland (CH)
          luakit_searx_instance_server="https://search.demoniak.ch"
          luakit_searx_instance_server_search="${luakit_searx_instance_server}/search?q=%s"
          ;;
        namejeff)
          # Switzerland (CH)
          luakit_searx_instance_server="https://searx.namejeff.xyz"
          luakit_searx_instance_server_search="${luakit_searx_instance_server}/searx/search?q=%s"
          ;;
        zhenyapav)
          # Iceland (IS)
          luakit_searx_instance_server="https://searx.zhenyapav.com"
          luakit_searx_instance_server_search="${luakit_searx_instance_server}/search?q=%s"
          ;;
      esac

      luakit_adblock_ctl=(
        "https://easylist.to/easylist/easylist.txt"
        "https://easylist.to/easylist/easyprivacy.txt"
        "https://secure.fanboy.co.nz/fanboy-annoyance.txt"
      )
    fi
  fi

  custom_core_pack_packages=""
  custom_aur_packages=(
    #"yay-bin"
  )
  openvpn_delevation_user=""
  openvpn_delevation_user_uid_gid=$(shuf -i 2000-65533 -n 1)
  crda_region_locale_split=$(grep -i "lang" /etc/locale.conf | sed "s/^LANG=//g")
  crda_region=$(echo "${crda_region_locale_split}" | sed "s/.*_//g" | sed "s/\..*//g")
  umask=077
  machine_id="xenos"
  os_release="linux"
  locale="${crda_region_locale_split}"

  return 0
}

configure_firewall(){
  # Configure the firewall
  sudo iptables -N TCP
  sudo iptables -N UDP
  sudo iptables -P FORWARD DROP
  sudo iptables -P OUTPUT ACCEPT
  sudo iptables -P INPUT DROP
  sudo iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
  sudo iptables -A INPUT -i lo -j ACCEPT
  sudo iptables -A INPUT -m conntrack --ctstate INVALID -j DROP
  sudo iptables -A INPUT -p icmp --icmp-type 8 -m conntrack --ctstate NEW -j ACCEPT
  sudo iptables -A INPUT -p udp -m conntrack --ctstate NEW -j UDP
  sudo iptables -A INPUT -p tcp --syn -m conntrack --ctstate NEW -j TCP
  sudo iptables -A INPUT -p udp -j REJECT --reject-with icmp-port-unreachable
  sudo iptables -A INPUT -p tcp -j REJECT --reject-with tcp-reset
  sudo iptables -A INPUT -j REJECT --reject-with icmp-proto-unreachable

  # Save our firewall rules
  sudo sed -i "d" /etc/iptables/iptables.rules
  sudo iptables-save | sudo tee -a /etc/iptables/iptables.rules > /dev/null

  # Enable and start the firewall
  sudo systemctl enable iptables.service
  sudo systemctl start iptables.service

  return 0
}

configure_makepkg_mkinitcpio_pacman(){
  local optimized_axel="/usr/bin/axel -n 2 -o %o %u"

  ## Configure makepkg, mkinitcpio and pacman
  # makepkg
  local sedctl=(
    "'http::.*|'http::${optimized_axel}'"
    "'https::.*|'https::${optimized_axel}'"
    "-march=x86-64 -mtune=generic|-march=native -mtune=native"
    "^RUSTFLAGS=.*|RUSTFLAGS=\"-C opt-level=2 -C target-cpu=native -C force-frame-pointers=yes\""
    "^MAKEFLAGS=.*|MAKEFLAGS=\"-j${cpu_cores}\""
    "^DEBUG_CFLAGS=.*|DEBUG_CFLAGS=\"\""
    "^DEBUG_CXXFLAGS=.*|DEBUG_CXXFLAGS=\"\""
    "^DEBUG_RUSTFLAGS=.*|DEBUG_RUSTFLAGS=\"\""
    "debug lto|!debug !lto"
    "^COMPRESSXZ=.*|COMPRESSXZ=(xz -c -z --threads=${cpu_cores} -)"
    "^PKGEXT=.*|PKGEXT='.pkg.tar.xz'"
  )

  _proto_sed_bulk "/etc/makepkg.conf" "${sedctl[@]}"

  # mkinitcpio
  sedctl=(
    "^COMPRESSION=\"xz\"|COMPRESSION=\"xz\""
    "^COMPRESSION_OPTIONS=.*|COMPRESSION_OPTIONS=(-c -z --threads=${cpu_cores} -)"
  )

  _proto_sed_bulk "/etc/mkinitcpio.conf" "${sedctl[@]}"

  # pacman
  sedctl=(
    "^XferCommand.*curl.*|XferCommand = ${optimized_axel}"
  )

  _proto_sed_bulk "/etc/pacman.conf" "${sedctl[@]}"

  return 0
}

install_essentials(){
  ### Begin core_pack generation
  ## Boilerplate
  # Base
  local core_pack="xorg-server xorg-server-devel xorg-xinit xorg-xinput xorg-xsetroot"

  # Graphic Drivers
  if [[ -n "${is_amd_gpu}" ]]; then
    core_pack="${core_pack} xf86-video-amdgpu"
  fi

  if [[ -n "${is_intel_gpu}" ]]; then
    core_pack="${core_pack} xf86-video-intel"
  fi

  if [[ -n "${is_nvidia_gpu}" && -n "${install_nvidia_driver}" ]]; then
    core_pack="${core_pack} nvidia-dkms"
  fi

  if [[ -n "${is_intel_gpu}" && -n "${is_nvidia_gpu}" && -n "${install_optimus_manager}" ]]; then
    _proto_makepkg "optimus-manager-git"

    sudo sed -i "s/^auto_logout=.*/auto_logout=no/g" /usr/share/optimus-manager.conf
    sudo sed -i "s/^startup_mode=.*/startup_mode=intel/g" /usr/share/optimus-manager.conf
    sudo sed -i "s/^startup_auto_battery_mode=.*/startup_auto_battery_mode=intel/g" /usr/share/optimus-manager.conf
    sudo sed -i "s/^startup_auto_extpower_mode=.*/startup_auto_extpower_mode=intel/g" /usr/share/optimus-manager.conf
    sudo sed -i "s/^driver=.*/driver=intel/g" /usr/share/optimus-manager.conf
    sudo sed -i "s/^accel=.*/accel=sna/g" /usr/share/optimus-manager.conf
    sudo sed -i "s/^tearfree=.*/tearfree=yes/g" /usr/share/optimus-manager.conf
    sudo sed -i "s/^DRI=.*/DRI=3/g" /usr/share/optimus-manager.conf
  fi

  if [[ -n "${is_amd_gpu}" ]]; then
    sed -i "s/Identifier.*/Identifier \"amd\"/g" etc/X11/xorg.conf.d/00_gpu_configuration.conf
    sed -i "s/Driver.*/Driver \"amdgpu\"/g" etc/X11/xorg.conf.d/00_gpu_configuration.conf
    sed -i "s/Option.*AccelMethod.*/Option \"AccelMethod\" \"glamor\"/g" etc/X11/xorg.conf.d/00_gpu_configuration.conf

    sudo cp etc/X11/xorg.conf.d/00_gpu_configuration.conf /etc/X11/xorg.conf.d/
  fi

  if [[ -n "${is_intel_gpu}" && -z "${install_optimus_manager}" ]]; then
    sed -i "s/Identifier.*/Identifier \"intel\"/g" etc/X11/xorg.conf.d/00_gpu_configuration.conf
    sed -i "s/Driver.*/Driver \"intel\"/g" etc/X11/xorg.conf.d/00_gpu_configuration.conf
    sed -i "s/Option.*AccelMethod.*/Option \"AccelMethod\" \"sna\"/g" etc/X11/xorg.conf.d/00_gpu_configuration.conf

    sudo cp etc/X11/xorg.conf.d/00_gpu_configuration.conf /etc/X11/xorg.conf.d/
  fi

  # GUI (1/2)
  # 2bwm utilizing Terminator as terminal
  core_pack="${core_pack} terminator"

  ## Programs by category
  # Audio and video
  core_pack="${core_pack} alsa-utils"

  if [[ -z "${install_only_essentials}" ]]; then
    core_pack="${core_pack} mpv"
  fi
  # Archiver
  core_pack="${core_pack} bzip2 gzip p7zip tar xz zip unzip"
  # Cleaner
  core_pack="${core_pack} bleachbit"
  # Download utilities
  if [[ -z "${install_only_essentials}" ]]; then
    core_pack="${core_pack} deluge-gtk ffmpeg yt-dlp"
  fi
  # Graphics
  if [[ -z "${install_only_essentials}" ]]; then
    core_pack="${core_pack} gimp inkscape pqiv"
  fi
  # Misc utilities
  core_pack="${core_pack} bash-completion brightnessctl man-db man-pages pacman-contrib rsync slock tree xautolock xbindkeys xwallpaper"

  if [[ -z "${install_only_essentials}" ]]; then
    core_pack="${core_pack} neofetch reflector"
  fi
  # Mounting
  core_pack="${core_pack} ntfs-3g udiskie"

  if [[ -z "${install_only_essentials}" ]]; then
    core_pack="${core_pack} dosfstools"
  fi
  # Networking
  core_pack="${core_pack} crda networkmanager nm-connection-editor"
  # Office
  core_pack="${core_pack} howl"

  if [[ -z "${install_only_essentials}" ]]; then
    core_pack="${core_pack} libreoffice-fresh mupdf"
  fi
  # Security
  core_pack="${core_pack} bubblewrap haveged opendoas pwgen rng-tools"
  # Soft dependencies not linked in core packages
  core_pack="${core_pack} gnome-keyring gnome-themes-extra gtk-engines gtk-engine-murrine libsecret"
  # System auditing
  if [[ -z "${install_only_essentials}" ]]; then
    core_pack="${core_pack} arch-audit lynis"
  fi
  # Themeing
  core_pack="${core_pack} arc-gtk-theme papirus-icon-theme noto-fonts noto-fonts-cjk noto-fonts-emoji noto-fonts-extra xcursor-vanilla-dmz"
  # Thermald
  if [[ -n "${is_intel_gpu}" ]]; then
    core_pack="${core_pack} thermald"
  fi
  # TPM 2.0
  if [[ -n "${has_tpm}" && "${has_tpm}" == 2 ]]; then
    core_pack="${core_pack} ccid opensc tpm2-abrmd tpm2-pkcs11 tpm2-tools"
  fi
  # Web Browser (2/3)
  if [[ -n "${custom_web_browser}" ]]; then
    if [[ "${custom_web_browser}" == "luakit-git" ]]; then
      # Soft dependencies not linked in PKGBUILD
      core_pack="${core_pack} lua51-filesystem luajit webkit2gtk webkit2gtk-4.1"

      # Setup configs
      mkdir -p "$HOME/.config/luakit/"

      cp tilde/config/luakit/rc.lua "$HOME/.config/luakit/"

      if [[ -n "${custom_user_agent}" ]]; then
        sed -i "s|^settings.webview.user_agent =.*|settings.webview.user_agent = \"${custom_user_agent}\"|g" tilde/config/luakit/userconf.lua
      fi

      sed -i "s|^settings.window.home_page =.*|settings.window.home_page = \"${luakit_searx_instance_server}\"|g" tilde/config/luakit/userconf.lua
      sed -i "s|^engines.searx =.*|engines.searx = \"${luakit_searx_instance_server_search}\"|g" tilde/config/luakit/userconf.lua

      cp tilde/config/luakit/userconf.lua "$HOME/.config/luakit/"

      # Setup adblock
      mkdir -p "$HOME/.local/share/luakit/adblock/"

      for ctl in "${luakit_adblock_ctl[@]}"
      do
        curl -o "$HOME/.local/share/luakit/adblock/${ctl##*/}" "${ctl}" && sleep 3
      done
    fi
  fi

  if [[ -n "${custom_core_pack_packages}" ]]; then
    core_pack="${core_pack} ${custom_core_pack_packages}"
  fi

  ### Begin install
  # Install core_pack
  sudo pacman -S --noconfirm $core_pack

  # GUI (2/2)
  _proto_makepkg "2bwm-git"

  # Web Browser (3/3)
  if [[ -n "${custom_web_browser}" ]]; then
    _proto_makepkg "${custom_web_browser}"
  fi

  # Custom Aur Packages
  if [[ -n "${custom_aur_packages[@]}" ]]; then
    for package in "${custom_aur_packages[@]}"
    do
      _proto_makepkg "${package}"
    done
  fi

  return 0
}

install_optionals(){
  if [[ -n "${openvpn_delevation_user}" ]]; then
    # Install openvpn-update-systemd-resolved-git
    _proto_makepkg "openvpn-update-systemd-resolved-git"
    sudo sed -i "s/openvpn_delevation_user/${openvpn_delevation_user}/g" /usr/share/polkit-1/rules.d/00-openvpn-update-systemd-resolved.rules

    # Setup the openvpn delevation user
    sudo useradd -r -c "Openvpn delevation user" -u "${openvpn_delevation_user_uid_gid}" -s /usr/bin/nologin -d / "${openvpn_delevation_user}"
    sudo groupmod -g "${openvpn_delevation_user_uid_gid}" "${openvpn_delevation_user}"

    # Create a shared directory for .ovpn files
    sudo mkdir -p /home/shared
  fi

  # Install redshift-minimal
  _proto_makepkg "redshift-minimal"

  # Setup xenos-.*
  local xenosctl=(
    "xenos-control-defaults"
    "xenos-control-dns"
    "xenos-null"
    "xenos-setup-power-scheme"
    "xenos-setup-time"
  )

  xenos_pack=()

  for ctl in "${xenosctl[@]}"
  do
    if [[ -f "usr/bin/${ctl}.sh" ]]; then
      sudo cp "usr/bin/${ctl}.sh" /usr/bin/

      if [[ "${ctl}" == "xenos-null" ]]; then
        sudo chmod 755 "/usr/bin/${ctl}.sh"

        mkdir -p "$HOME/.local/share/applications"

        cp factory/mimeapps.list "$HOME/.config/"
        cp factory/mimeapps.list "$HOME/.local/share/applications/"
        sudo cp factory/mimeapps.list /etc/xdg/

        chmod 600 "$HOME/.config/mimeapps.list"
        chmod 600 "$HOME/.local/share/applications/mimeapps.list"
        sudo chmod 600 /etc/xdg/mimeapps.list

        sudo chattr +i "$HOME/.config/mimeapps.list"
        sudo chattr +i "$HOME/.local/share/applications/mimeapps.list"
        sudo chattr +i /etc/xdg/mimeapps.list

        sudo cp "usr/share/applications/${ctl}.desktop" /usr/share/applications/
        sudo chmod 644 "/usr/share/applications/${ctl}.desktop"
        sudo chattr +i "/usr/share/applications/${ctl}.desktop"
      else
        sudo chmod 700 "/usr/bin/${ctl}.sh"
      fi

      sudo chattr +i "/usr/bin/${ctl}.sh"
    fi

    if [[ -f "etc/systemd/system/${ctl}.service" ]]; then
      sudo cp "etc/systemd/system/${ctl}.service" /etc/systemd/system/

      xenos_pack=("${xenos_pack[@]}" "${ctl}.service")
    fi
  done

  return 0
}

toggle_systemctl(){
  #
  ## Variable prep
  #
  colord_installed=$(pacman -Qs colord | grep -i -v "lib" || echo "")

  ### Disable some unused services, sockets, targets and timers
  ## Start with dhcpcd, which is removed later
  sudo systemctl stop dhcpcd.service
  sudo systemctl disable dhcpcd.service

  # avahi (1/3)
  local disablectl=(
    "alsa"
    "archlinux"
    "audit-rules"
    "auditd"
    "avahi"
    "bluetooth"
    "debug-shell"
    "dirmngr"
    "emergency"
    "gcr-ssh-agent"
    "git-daemon"
    "gpg-agent"
    "keyboxd"
    "NetworkManager-dispatcher"
    "NetworkManager-wait-online"
    "nscd"
    "p11-kit-server"
    "printer"
    "reflector"
    "remote"
    "rescue"
    "rfkill"
    "rsyncd"
    "sleep"
    "suspend"
    "syslog"
    "systemd-network-generator"
    "time-set"
    "time-sync"
    "wpa_supplicant-nl80211@"
    "wpa_supplicant-wired@"
    "wpa_supplicant@"
  )

  if [[ -n "${colord_installed}" ]]; then
    disablectl=("${disablectl[@]}" "colord")
  fi

  local disablectl_result=()

  for ctl in "${disablectl[@]}"
  do
    disablectl_result=("${disablectl_result[@]}" $(find /usr/lib/systemd/system/ -type f -iname "*${ctl}*"))
    disablectl_result=("${disablectl_result[@]}" $(find /usr/lib/systemd/user/ -type f -iname "*${ctl}*"))
  done

  for ctl in "${disablectl_result[@]}"
  do
    if [[ "${ctl}" == *systemd/system* ]]; then
      sudo systemctl mask "${ctl##*/}"
    elif [[ "${ctl}" == *systemd/user* ]]; then
      systemctl mask --user "${ctl##*/}"
    fi
  done

  # Note: Here we need to unmask gpg-agent* in the user context for its day to day usage
  systemctl unmask --user gpg-agent.service
  systemctl unmask --user gpg-agent.socket

  ### Enable all necessary services, sockets, targets and timers
  ## This consists of all items installed with core_pack and xenos_pack from S1.sh and S2.sh.
  local enablectl=(
    "apparmor.service"
    "haveged.service"
    "NetworkManager.service"
    "rngd.service"
    "systemd-resolved.service"
  )

  if [[ -n "${is_intel_gpu}" ]]; then
    enablectl=("${enablectl[@]}" "upower.service" "thermald.service")
  fi

  if [[ -n "${is_intel_gpu}" && -n "${is_nvidia_gpu}" && -n "${install_optimus_manager}" ]]; then
    enablectl=("${enablectl[@]}" "optimus-manager.service")
  fi

  if [[ -n "${has_tpm}" && "${has_tpm}" == 2 ]]; then
    enablectl=("${enablectl[@]}" "tpm2-abrmd.service" "pcscd.service")
  fi

  if [[ -n "${xenos_pack[@]}" ]]; then
    enablectl=("${enablectl[@]}" "${xenos_pack[@]}")
  fi

  for ctl in "${enablectl[@]}"
  do
    sudo systemctl enable "${ctl}"
  done

  return 0
}

misc_fixes(){
  # Fix apparmor boot time hanging issue
  sudo sed -i "s/^#write-cache/write-cache/g" /etc/apparmor/parser.conf
  sudo sed -i "s/^#Optimize=compress-fast/Optimize=compress-fast/g" /etc/apparmor/parser.conf

  # Fix lm_sensors
  sudo sensors-detect --auto

  # Fix NetworkManager issues
  sudo cp etc/NetworkManager/NetworkManager.conf /etc/NetworkManager/

  # Setup our specific CRDA region
  sed -i "s/ieee80211_regdom=/ieee80211_regdom=${crda_region}/g" etc/modprobe.d/optional/10_vendor_any.conf
  sudo sed -i "s/^#WIRELESS_REGDOM=\"${crda_region}\"/WIRELESS_REGDOM=\"${crda_region}\"/g" /etc/conf.d/wireless-regdom
  echo -e "# Set CRDA region\ncountry=${crda_region}" | sudo tee -a /etc/wpa_supplicant/wpa_supplicant.conf > /dev/null

  # Setup our modprobe.d driver customizations
  sudo cp etc/modprobe.d/optional/10_vendor_any.conf /etc/modprobe.d/

  if [[ -n "${is_intel_gpu}" ]]; then
    sudo cp etc/modprobe.d/optional/11_vendor_intel.conf /etc/modprobe.d/
  else
    sudo cp etc/modprobe.d/optional/11_vendor_amd.conf /etc/modprobe.d/
  fi

  return 0
}

harden_systemd_parts(){
  # Harden /etc/systemd/journald.conf
  if [[ -f "/etc/systemd/journald.conf" ]]; then
    sudo sed -i "s/^#Storage=.*/Storage=persistent/g" /etc/systemd/journald.conf
    sudo sed -i "s/^#Compress=.*/Compress=yes/g" /etc/systemd/journald.conf
    sudo sed -i "s/^#SystemMaxUse=.*/SystemMaxUse=50M/g" /etc/systemd/journald.conf
  fi

  # Harden /etc/systemd/logind.conf
  if [[ -f "/etc/systemd/logind.conf" ]]; then
    sudo sed -i "s/^#HandleSuspendKey=.*/HandleSuspendKey=ignore/g" /etc/systemd/logind.conf
    sudo sed -i "s/^#HandleSuspendKeyLongPress=.*/HandleSuspendKeyLongPress=ignore/g" /etc/systemd/logind.conf
    sudo sed -i "s/^#HandleHibernateKey=.*/HandleHibernateKey=ignore/g" /etc/systemd/logind.conf
    sudo sed -i "s/^#HandleHibernateKeyLongPress=.*/HandleHibernateKeyLongPress=ignore/g" /etc/systemd/logind.conf
    sudo sed -i "s/^#HandleLidSwitch=.*/HandleLidSwitch=ignore/g" /etc/systemd/logind.conf
    sudo sed -i "s/^#HandleLidSwitchExternalPower=.*/HandleLidSwitchExternalPower=ignore/g" /etc/systemd/logind.conf
    sudo sed -i "s/^#HandleLidSwitchDocked=.*/HandleLidSwitchDocked=ignore/g" /etc/systemd/logind.conf
  fi

  # Harden /etc/systemd/sleep.conf
  if [[ -f "/etc/systemd/sleep.conf" ]]; then
    sudo sed -i "s/^#AllowSuspend=.*/AllowSuspend=no/g" /etc/systemd/sleep.conf
    sudo sed -i "s/^#AllowHibernation=.*/AllowHibernation=no/g" /etc/systemd/sleep.conf
    sudo sed -i "s/^#AllowSuspendThenHibernate=.*/AllowSuspendThenHibernate=no/g" /etc/systemd/sleep.conf
    sudo sed -i "s/^#AllowHybridSleep=.*/AllowHybridSleep=no/g" /etc/systemd/sleep.conf
  fi

  # Harden /etc/systemd/system.conf and /etc/systemd/user.conf
  if [[ -f "/etc/systemd/system.conf" && -f "/etc/systemd/user.conf" ]]; then
    sudo sed -i "s/^#DumpCore=.*/DumpCore=no/g" /etc/systemd/system.conf
    sudo sed -i "s/^#ShowStatus=.*/ShowStatus=no/g" /etc/systemd/system.conf
    sudo sed -i "s/^#CrashChangeVT=.*/CrashChangeVT=no/g" /etc/systemd/system.conf
    sudo sed -i "s/^#CrashShell=.*/CrashShell=no/g" /etc/systemd/system.conf
    sudo sed -i "s/^#SystemCallArchitectures=.*/SystemCallArchitectures=native/g" /etc/systemd/system.conf /etc/systemd/user.conf
    sudo sed -i "s/^#DefaultTimeoutStartSec=.*/DefaultTimeoutStartSec=10s/g"  /etc/systemd/system.conf /etc/systemd/user.conf
    sudo sed -i "s/^#DefaultTimeoutStopSec=.*/DefaultTimeoutStopSec=10s/g"  /etc/systemd/system.conf /etc/systemd/user.conf
    sudo sed -i "s/^#DefaultDeviceTimeoutSec=.*/DefaultDeviceTimeoutSec=10s/g"  /etc/systemd/system.conf /etc/systemd/user.conf
    sudo sed -i "s/^#DefaultLimitCORE=.*/DefaultLimitCORE=0/g" /etc/systemd/system.conf /etc/systemd/user.conf
  fi

  # Harden /etc/udev/udev.conf
  if [[ -f "/etc/udev/udev.conf" ]]; then
    sudo sed -i "s/^#event_timeout=.*/event_timeout=10/g" /etc/udev/udev.conf
    sudo sed -i "s/^#timeout_signal=.*/timeout_signal=SIGKILL/g" /etc/udev/udev.conf
  fi

  # Harden systemd-resolved network stack
  sudo systemctl start systemd-resolved.service
  sudo ln -snf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
  sudo systemctl stop systemd-resolved.service

  # Harden services at /etc/systemd/system/
  sudo cp -R usr/lib/systemd/system/ /etc/systemd/

  if [[ -n "${is_intel_gpu}" ]]; then
    sudo cp usr/lib/systemd/system-optional/upower.service /etc/systemd/system/
    sudo cp usr/lib/systemd/system-optional/thermald.service /etc/systemd/system/
  fi

  if [[ -n "${is_intel_gpu}" && -n "${is_nvidia_gpu}" && -n "${install_optimus_manager}" ]]; then
    sudo cp usr/lib/systemd/system-optional/optimus-manager.service /etc/systemd/system/
  fi

  if [[ -n "${has_tpm}" && "${has_tpm}" == 2 ]]; then
    sudo cp usr/lib/systemd/system-optional/tpm2-abrmd.service /etc/systemd/system/
    sudo cp usr/lib/systemd/system-optional/pcscd.service /etc/systemd/system/
  fi

  sudo cp -R usr/lib/systemd/user/ /etc/systemd/

  # Harden systemd binary leftovers
  local systemdctl=(
    "/usr/bin/rfkill"
    "/usr/lib/systemd/system-generators/systemd-ssh-generator"
    "/usr/lib/systemd/systemd-network-generator"
    "/usr/lib/systemd/systemd-sleep"
    "/usr/lib/systemd/systemd-socket-proxyd"
    "/usr/lib/systemd/systemd-ssh-proxy"
  )

  _proto_purge_loop "${systemdctl[*]}"

  return 0
}

harden_other_parts(){
  # Harden at-spi* or accessibility (1/2)
  local atspictl=(
    "/etc/xdg/Xwayland-session.d/00-at-spi"
    "/usr/lib/at-spi-bus-launcher"
    "/usr/lib/at-spi2-registryd"
    "/usr/lib/gnome-settings-daemon-3.0/gtk-modules/at-spi2-atk.desktop"
    "/usr/lib/systemd/user/at-spi-dbus-bus.service"
  )

  _proto_purge_loop "${atspictl[*]}"

  # Harden avahi (2/3)
  local avahictl=(
    "/usr/bin/avahi-autoipd"
    "/usr/bin/avahi-bookmarks"
    "/usr/bin/avahi-browse"
    "/usr/bin/avahi-daemon"
    "/usr/bin/avahi-discover"
    "/usr/bin/avahi-discover-standalone"
    "/usr/bin/avahi-dnsconfd"
    "/usr/bin/avahi-publish"
    "/usr/bin/avahi-resolve"
    "/usr/bin/avahi-set-host-name"
    "/usr/bin/bshell"
    "/usr/bin/bssh"
    "/usr/bin/bvnc"
    "/usr/share/applications/avahi-discover.desktop"
    "/usr/share/applications/bssh.desktop"
    "/usr/share/applications/bvnc.desktop"
  )

  _proto_purge_loop "${avahictl[*]}"

  # Harden consoles and ttys
  echo -e "\n+:(wheel):LOCAL\n-:ALL:ALL" | sudo tee -a /etc/security/access.conf > /dev/null
  sudo sed -i "1,2!d" /etc/securetty

  ## Harden dbus related items
  # at-spi* or accessibility (2/2)
  # avahi (3/3)
  local dbusctl=(
    "/usr/share/dbus-1/accessibility-services/org.a11y.atspi.Registry.service"
    "/usr/share/dbus-1/services/org.a11y.Bus.service"
    "/usr/share/dbus-1/system-services/org.freedesktop.Avahi.service"
    "/usr/share/dbus-1/system-services/org.freedesktop.nm_dispatcher.service"
  )

  if [[ -n "${colord_installed}" ]]; then
    dbusctl=("${dbusctl[@]}" "/usr/share/dbus-1/services/org.freedesktop.ColorHelper.service" "/usr/share/dbus-1/system-services/org.freedesktop.ColorManager.service")
  fi

  _proto_purge_loop "${dbusctl[*]}"

  # Harden faillock.conf
  # 3 login attempts within 5 minutes results in a 10 minute lockout
  echo -e "\ndeny = 3\nfail_interval = 300\nunlock_time = 600" | sudo tee -a  /etc/security/faillock.conf > /dev/null

  # Harden issue
  sudo sed -i "d" /etc/issue
  echo -e "Unauthorized access to this machine is prohibited.\nAll access is logged and monitored.\n " | sudo tee -a /etc/issue > /dev/null

  # Harden limits.conf
  sudo sed -i "s/^# End of file//g" /etc/security/limits.conf
  echo -e "* hard core 0\n* soft core 0\n* hard maxsyslogins 1\n\n# End of file" | sudo tee -a  /etc/security/limits.conf > /dev/null

  ## Harden login.defs
  # Umask (1/2)
  sudo sed -i "s/^UMASK.*/UMASK ${umask}/g" /etc/login.defs
  # Password aging
  sudo sed -i "s/^PASS_MAX_DAYS.*/PASS_MAX_DAYS 120/g" /etc/login.defs
  sudo sed -i "s/^PASS_MIN_DAYS.*/PASS_MIN_DAYS 1/g" /etc/login.defs
  sudo sed -i "s/^PASS_WARN_AGE.*/PASS_WARN_AGE 14/g" /etc/login.defs

  # Harden machine-id
  case $machine_id in
    whonix)
      machine_id="b08dfa6083e7567a1921a715000001fb"
      ;;
    xenos)
      machine_id="1b84e8d8a2c4f4d84456395a63bcb3c1"
      ;;
  esac

  sudo sed -i "d" /etc/machine-id /var/lib/dbus/machine-id
  echo "${machine_id}" | sudo tee -a /etc/machine-id /var/lib/dbus/machine-id > /dev/null

  ## Harden modules
  # Blacklist
  for modprobe in etc/modprobe.d/required/*
  do
    sudo cp "${modprobe}" /etc/modprobe.d/
  done
  # Whitelist
  sudo cp etc/modules-load.d/00_whitelist_lts.conf /etc/modules-load.d/

  ## Harden mount options
  # Ensure /var/log/audit exists
  if [[ ! -d "/var/log/audit" ]]; then
    mkdir -p "/var/log/audit"
  fi

  # /dev/sdx2 or /
  sudo sed -i "6 s/rw,relatime/defaults/g" /etc/fstab
  # /dev/sdx1 or /boot
  sudo sed -i "9 s/rw,relatime,fmask=0022,dmask=0022/defaults,nosuid,nodev,noexec,fmask=0077,dmask=0077/g" /etc/fstab

  local fstab_string_start=$(cat /dev/urandom | tr -dc "a-z0-9" | fold -w 11 | head -n 1)
  fstab_string_start="/xenos-${fstab_string_start}"
  local fstab_string_end=($(cat /dev/urandom | tr -dc "a-zA-Z0-9" | fold -w 6 | head -n 7))
  local fstab_safe_bind_options="defaults,private,bind"
  local fstab_proc_group="proc"
  local fstabctl=(
    "/etc ${fstab_string_start}-etc-${fstab_string_end[0]} none ${fstab_safe_bind_options},nodev 0 0"
    "/usr ${fstab_string_start}-usr-${fstab_string_end[1]} none ${fstab_safe_bind_options},nodev 0 0"
    "/var ${fstab_string_start}-var-1-${fstab_string_end[2]} none ${fstab_safe_bind_options},nosuid,nodev 0 0"
    "/var/log ${fstab_string_start}-var-2-${fstab_string_end[3]}/log none ${fstab_safe_bind_options},nosuid,nodev,noexec 0 0"
    "/var/log/audit ${fstab_string_start}-var-3-${fstab_string_end[4]}/log/audit none ${fstab_safe_bind_options},nosuid,nodev,noexec 0 0"
    "/var/tmp ${fstab_string_start}-var-4-${fstab_string_end[5]}/tmp none ${fstab_safe_bind_options},nosuid,nodev,noexec 0 0"
    "/home ${fstab_string_start}-home-${fstab_string_end[6]} none ${fstab_safe_bind_options},nosuid,nodev,noexec 0 0"
    "tmpfs /tmp tmpfs defaults,nosuid,nodev,noexec 0 0"
    "tmpfs /dev/shm tmpfs defaults,nosuid,nodev,noexec 0 0"
    "proc /proc proc nosuid,nodev,noexec,hidepid=2,gid=${fstab_proc_group} 0 0"
  )

  echo -e "\n" | sudo tee -a  /etc/fstab > /dev/null

  for ctl in "${fstabctl[@]}"
  do
    echo "${ctl}" | sudo tee -a  /etc/fstab > /dev/null
  done

  sudo mkdir -p /etc/systemd/system/systemd-logind.service.d/
  echo -e "[Service]\nSupplementaryGroups=${fstab_proc_group}" | sudo tee -a  /etc/systemd/system/systemd-logind.service.d/00_hide_pid.conf  > /dev/null

  # Harden os-release
  sudo cp "usr/lib/os-release-${os_release}" /usr/lib/os-release

  ## Harden pam.d
  # SU elevation, even though SU will be disabled by locking root lets restrict it anyways
  sudo sed -i "6 s/^#auth/auth/g" /etc/pam.d/su /etc/pam.d/su-l
  # Disable pam_motd.so and pam_mail.so
  sudo sed -i "s/^.*pam_motd.so.*//g" /etc/pam.d/system-login
  sudo sed -i "s/^.*pam_mail.so.*//g" /etc/pam.d/system-login
  # Disable pam_systemd_home.so (systemd-homed intergration)
  sudo sed -i "s/^.*pam_systemd_home.so.*//g" /etc/pam.d/system-auth

  ## Harden profile
  # Umask (2/2)
  sed -i "s/^umask.*/umask ${umask}/g" factory/profile-bashrc-template
  # Locale
  sed -i "s/LOCALE/${locale}/g" factory/profile-bashrc-template
  sudo cp factory/profile-bashrc-template /etc/profile

  # Harden root account
  sudo passwd -l root

  # Harden ssh
  echo -e "\nHost *\n   AddressFamily inet\n   Compression no\n   EnableSSHKeysign no\n   ForwardAgent no\n   ForwardX11 no\n   ForwardX11Trusted no\n   PermitLocalCommand no" | sudo tee -a /etc/ssh/ssh_config > /dev/null

  # Harden sshd
  local sshdport=$(shuf -i 200-18500 -n 1)

  sudo sed -i "s/^#Port.*/Port ${sshdport}/g"  /etc/ssh/sshd_config
  sudo sed -i "s/^#PermitRootLogin.*/PermitRootLogin no/g"  /etc/ssh/sshd_config
  sudo sed -i "s/^#StrictModes.*/StrictModes yes/g"  /etc/ssh/sshd_config
  sudo sed -i "s/^#HostbasedAuthentication.*/HostbasedAuthentication no/g"  /etc/ssh/sshd_config
  sudo sed -i "s/^#IgnoreUserKnownHosts.*/IgnoreUserKnownHosts yes/g"  /etc/ssh/sshd_config
  sudo sed -i "s/^#IgnoreRhosts.*/IgnoreRhosts yes/g"  /etc/ssh/sshd_config
  sudo sed -i "s/^#PermitEmptyPasswords.*/PermitEmptyPasswords no/g"  /etc/ssh/sshd_config
  sudo sed -i "s/^#KerberosAuthentication.*/KerberosAuthentication no/g"  /etc/ssh/sshd_config
  sudo sed -i "s/^#GSSAPIAuthentication.*/GSSAPIAuthentication no/g"  /etc/ssh/sshd_config
  sudo sed -i "s/^#AllowAgentForwarding.*/AllowAgentForwarding no/g"  /etc/ssh/sshd_config
  sudo sed -i "s/^#AllowTcpForwarding.*/AllowTcpForwarding no/g"  /etc/ssh/sshd_config
  sudo sed -i "s/^#X11Forwarding.*/X11Forwarding no/g"  /etc/ssh/sshd_config
  sudo sed -i "s/^#PermitTTY.*/PermitTTY no/g"  /etc/ssh/sshd_config
  sudo sed -i "s/^#TCPKeepAlive.*/TCPKeepAlive no/g"  /etc/ssh/sshd_config
  sudo sed -i "s/^#PermitUserEnvironment.*/PermitUserEnvironment no/g"  /etc/ssh/sshd_config
  sudo sed -i "s/^#Compression.*/Compression no/g"  /etc/ssh/sshd_config
  sudo sed -i "s/^#UseDNS.*/UseDNS no/g"  /etc/ssh/sshd_config
  sudo sed -i "s|^#Banner.*|Banner /etc/issue|g"  /etc/ssh/sshd_config

  # Harden sysctl
  sudo cp etc/sysctl.d/00_kernel_hardening.conf /etc/sysctl.d/

  # Harden xorg
  mkdir -p "$HOME/.local/share/xorg/"
  cp tilde/local/share/xorg/00_xorg_hardening.conf "$HOME/.local/share/xorg/"

  return 0
}

finalize(){
  # Fix annoying permission warning for /etc/systemd/*
  sudo find /etc/systemd/system/ -type f -execdir chmod 644 {} +
  sudo find /etc/systemd/user/ -type f -execdir chmod 644 {} +

  # Regenerate mkinitcpio
  sudo mkinitcpio -P

  # Setup .axelrc
  if [[ -n "${custom_user_agent}" ]]; then
    sed -i "s|^user_agent =.*|user_agent = \"${custom_user_agent}\"|g" factory/axelrc
    cp factory/axelrc "$HOME/.axelrc"
    sudo cp factory/axelrc /etc/
  fi

  # Setup .bash_profile
  cp tilde/bash_profile "$HOME/.bash_profile"

  # Setup .bashrc
  sudo cp factory/profile-bashrc-template /etc/skel/.bashrc
  cp factory/profile-bashrc-template "$HOME/.bashrc"

  # Setup .curlrc
  if [[ -n "${custom_user_agent}" ]]; then
    sed -i "s|^--user-agent.*|--user-agent \"${custom_user_agent}\"|g" factory/curlrc
    cp factory/curlrc "$HOME/.curlrc"
    sudo cp factory/curlrc /etc/
  fi

  # Setup doas
  echo "permit :wheel" | sudo tee -a /etc/doas.conf > /dev/null

  # Setup gpg-agent.conf
  mkdir -p "$HOME/.gnupg/"
  cp tilde/gnupg/gpg-agent.conf "$HOME/.gnupg/"

  # Setup immutable recently-used.xbel
  # This patches a potential file permission bypass which grants access to the home directory
  rm -f "$HOME/.local/share/recently-used.xbel"
  touch "$HOME/.local/share/recently-used.xbel"
  sudo chattr +i "$HOME/.local/share/recently-used.xbel"

  # Setup null pointers
  # This patches potential data leaks for applications which have poor implementations of stdout and stderr redirection
  mkdir -p "$HOME/.howl/system"

  local nullctl=(
    "$HOME/.howl/system/command_line_history.lua"
    "$HOME/.howl/system/session.lua"
    "$HOME/.local/share/xorg/Xorg.0.log"
    "$HOME/.local/share/xorg/Xorg-stdout-stderr.log"
  )

  for ctl in "${nullctl[@]}"
  do
    rm -f "${ctl}"
    ln -s /dev/null "${ctl}"
  done

  # Setup .xinitrc
  cp tilde/xinitrc "$HOME/.xinitrc"

  # Remove orphaned packages
  sudo pacman -Rns $(pacman -Qtdq) --noconfirm

  # Remove obsolete core_pack packages
  sudo pacman -Rns --noconfirm dhcpcd

  # Remove obsolete core_pack dependencies forcefully
  sudo sed -i "s/^#IgnorePkg   =/IgnorePkg   = sudo/g"  /etc/pacman.conf
  sudo pacman -Rddns --noconfirm sudo

  return 0
}

exit_installer(){
  # Prompt for shutdown
  read -p "Xenos post install complete. Press [Enter] key to shutdown..."
  systemctl poweroff

  return 0
}


initialize
configure_firewall
configure_makepkg_mkinitcpio_pacman
install_essentials
install_optionals
toggle_systemctl
misc_fixes
harden_systemd_parts
harden_other_parts
finalize
exit_installer

exit 0

