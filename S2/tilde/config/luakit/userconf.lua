-------------------------------------------------
--
--           _______  _        _______  _______
-- |\     /|(  ____ \( (    /|(  ___  )(  ____ \
-- ( \   / )| (    \/|  \  ( || (   ) || (    \/
--  \ (_) / | (__    |   \ | || |   | || (_____
--   ) _ (  |  __)   | (\ \) || |   | |(_____  )
--  / ( ) \ | (      | | \   || |   | |      ) |
-- ( /   \ )| (____/\| )  \  || (___) |/\____) |
-- |/     \|(_______/|/    )_)(_______)\_______)
--
--
-- This file is a part of the Xenos Install Kit.
-- It adheres to the GNU GPL license.
--
-- https://gitlab.com/xenos-install-kit/xenos-install-kit
--
-- © 2020-2024
--
--
-------------------------------------------------


local settings = require "settings"
settings.application.enable_pdfjs = false
settings.application.prefer_dark_mode = false
settings.completion.max_items = 10
settings.webview.allow_file_access_from_file_urls = false
settings.webview.allow_modal_dialogs = false
settings.webview.allow_universal_access_from_file_urls = false
settings.webview.auto_load_images = true
settings.webview.default_charset = "utf-8"
settings.webview.draw_compositing_indicators = false
settings.webview.enable_accelerated_2d_canvas = false
settings.webview.enable_caret_browsing = false
settings.webview.enable_developer_extras = false
settings.webview.enable_dns_prefetching = false
settings.webview.enable_frame_flattening = false
settings.webview.enable_fullscreen = false
settings.webview.enable_html5_database = true
settings.webview.enable_html5_local_storage = true
settings.webview.enable_hyperlink_auditing = false
settings.webview.enable_java = false
settings.webview.enable_javascript = false
settings.webview.enable_media_stream = false
settings.webview.enable_mediasource = false
settings.webview.enable_offline_web_application_cache = false
settings.webview.enable_page_cache = false
settings.webview.enable_plugins = true
settings.webview.enable_resizable_text_areas = false
settings.webview.enable_site_specific_quirks = true
settings.webview.enable_smooth_scrolling = false
settings.webview.enable_spatial_navigation = false
settings.webview.enable_tabs_to_links = false
settings.webview.enable_webaudio = false
settings.webview.enable_webgl = false
settings.webview.enable_write_console_messages_to_stdout = false
settings.webview.enable_xss_auditor = true
settings.webview.hardware_acceleration_policy = "never"
settings.webview.javascript_can_access_clipboard = false
settings.webview.javascript_can_open_windows_automatically = false
settings.webview.media_playback_allows_inline = true
settings.webview.media_playback_requires_gesture = false
settings.webview.print_backgrounds = false
settings.webview.user_agent =
settings.webview.zoom_text_only = false
settings.window.act_on_synthetic_keys = false
settings.window.check_filepath = false
settings.window.close_with_last_tab = true
settings.window.home_page =
settings.window.load_etc_hosts = false
settings.window.reuse_new_tab_pages = true

local engines = settings.window.search_engines
engines.searx =
engines.default = engines.searx

local noscript = require "noscript"
noscript.enable_scripts = false
noscript.enable_plugins = false
