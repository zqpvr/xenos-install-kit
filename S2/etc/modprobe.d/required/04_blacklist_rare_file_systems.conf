#################################################
#
#           _______  _        _______  _______
# |\     /|(  ____ \( (    /|(  ___  )(  ____ \
# ( \   / )| (    \/|  \  ( || (   ) || (    \/
#  \ (_) / | (__    |   \ | || |   | || (_____
#   ) _ (  |  __)   | (\ \) || |   | |(_____  )
#  / ( ) \ | (      | | \   || |   | |      ) |
# ( /   \ )| (____/\| )  \  || (___) |/\____) |
# |/     \|(_______/|/    )_)(_______)\_______)
#
#
# This file is a part of the Xenos Install Kit.
# It adheres to the GNU GPL license.
#
# https://gitlab.com/xenos-install-kit/xenos-install-kit
#
# © 2020-2024
#
#
#################################################


# Valid for linux 6.6.*

### Rare File Systems
## /lib/modules/$(uname -r)/kernel/fs/
# /lib/modules/$(uname -r)/kernel/fs/9p
install 9p /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/affs
install affs /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/afs
install kafs /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/befs
install befs /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/cachefiles
install cachefiles /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/ceph
install ceph /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/coda
install coda /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/cramfs
install cramfs /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/ecryptfs
install ecryptfs /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/erofs
install erofs /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/f2fs
install f2fs /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/fscache
install fscache /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/gfs2
install gfs2 /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/hfs
install hfs /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/hfsplus
install hfsplus /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/jbd2
install jbd2 /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/jffs2
install jffs2 /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/jfs
install jfs /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/minix
install minix /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/nfs
install blocklayoutdriver /usr/bin/xenos-null.sh
install nfs_layout_nfsv41_files /usr/bin/xenos-null.sh
install nfs_layout_flexfiles /usr/bin/xenos-null.sh
install nfs /usr/bin/xenos-null.sh
install nfsv2 /usr/bin/xenos-null.sh
install nfsv3 /usr/bin/xenos-null.sh
install nfsv4 /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/nfs_common
install grace /usr/bin/xenos-null.sh
install nfs_acl /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/nfsd
install nfsd /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/nilfs2
install nilfs2 /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/ntfs3
install ntfs3 /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/ocfs2
install ocfs2_nodemanager /usr/bin/xenos-null.sh
install ocfs2_dlm /usr/bin/xenos-null.sh
install ocfs2_dlmfs /usr/bin/xenos-null.sh
install ocfs2 /usr/bin/xenos-null.sh
install ocfs2_stack_o2cb /usr/bin/xenos-null.sh
install ocfs2_stackglue /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/omfs
install omfs /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/orangefs
install orangefs /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/reiserfs
install reiserfs /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/romfs
install romfs /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/smb
install cifs /usr/bin/xenos-null.sh
install cifs_arc4 /usr/bin/xenos-null.sh
install cifs_md4 /usr/bin/xenos-null.sh
install ksmbd /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/ubifs
install ubifs /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/udf
install udf /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/ufs
install ufs /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/fs/zonefs
install zonefs /usr/bin/xenos-null.sh

