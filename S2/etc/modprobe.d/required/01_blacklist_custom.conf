#################################################
#
#           _______  _        _______  _______
# |\     /|(  ____ \( (    /|(  ___  )(  ____ \
# ( \   / )| (    \/|  \  ( || (   ) || (    \/
#  \ (_) / | (__    |   \ | || |   | || (_____
#   ) _ (  |  __)   | (\ \) || |   | |(_____  )
#  / ( ) \ | (      | | \   || |   | |      ) |
# ( /   \ )| (____/\| )  \  || (___) |/\____) |
# |/     \|(_______/|/    )_)(_______)\_______)
#
#
# This file is a part of the Xenos Install Kit.
# It adheres to the GNU GPL license.
#
# https://gitlab.com/xenos-install-kit/xenos-install-kit
#
# © 2020-2024
#
#
#################################################


# Valid for linux 6.6.*

### Custom Additions
## Nouveau Driver
# /lib/modules/$(uname -r)/kernel/drivers/gpu/drm/nouveau/
install nouveau /usr/bin/xenos-null.sh
## Various Platform Specific Drivers
# /lib/modules/$(uname -r)/kernel/drivers/platform/
install chromeos_acpi /usr/bin/xenos-null.sh
install chromeos_laptop /usr/bin/xenos-null.sh
install chromeos_privacy_screen /usr/bin/xenos-null.sh
install chromeos_pstore /usr/bin/xenos-null.sh
install chromeos_tbmc /usr/bin/xenos-null.sh
install cros-ec-sensorhub /usr/bin/xenos-null.sh
install cros-ec-typec /usr/bin/xenos-null.sh
install cros_ec /usr/bin/xenos-null.sh
install cros_ec_chardev /usr/bin/xenos-null.sh
install cros_ec_debugfs /usr/bin/xenos-null.sh
install cros_ec_i2c /usr/bin/xenos-null.sh
install cros_ec_ishtp /usr/bin/xenos-null.sh
install cros_ec_lightbar /usr/bin/xenos-null.sh
install cros_ec_lpcs /usr/bin/xenos-null.sh
install cros_ec_spi /usr/bin/xenos-null.sh
install cros_ec_sysfs /usr/bin/xenos-null.sh
install cros_ec_uart /usr/bin/xenos-null.sh
install cros_hps_i2c /usr/bin/xenos-null.sh
install cros_kbd_led_backlight /usr/bin/xenos-null.sh
install cros_typec_switch /usr/bin/xenos-null.sh
install cros_usbpd_logger /usr/bin/xenos-null.sh
install cros_usbpd_notify /usr/bin/xenos-null.sh
install wilco_ec /usr/bin/xenos-null.sh
install wilco_ec_debugfs /usr/bin/xenos-null.sh
install wilco_ec_events /usr/bin/xenos-null.sh
install wilco_ec_telem /usr/bin/xenos-null.sh
install mlxreg-hotplug /usr/bin/xenos-null.sh
install mlxreg-io /usr/bin/xenos-null.sh
install mlxreg-lc /usr/bin/xenos-null.sh
install nvsw-sn2201 /usr/bin/xenos-null.sh
install surface_aggregator /usr/bin/xenos-null.sh
install surface3-wmi /usr/bin/xenos-null.sh
install surface3_power /usr/bin/xenos-null.sh
install surface_acpi_notify /usr/bin/xenos-null.sh
install surface_aggregator_cdev /usr/bin/xenos-null.sh
install surface_aggregator_hub /usr/bin/xenos-null.sh
install surface_aggregator_registry /usr/bin/xenos-null.sh
install surface_aggregator_tabletsw /usr/bin/xenos-null.sh
install surface_dtx /usr/bin/xenos-null.sh
install surface_gpe /usr/bin/xenos-null.sh
install surface_hotplug /usr/bin/xenos-null.sh
install surface_platform_profile /usr/bin/xenos-null.sh
install surfacepro3_button /usr/bin/xenos-null.sh
install acer-wireless /usr/bin/xenos-null.sh
install acer-wmi /usr/bin/xenos-null.sh
install acerhdf /usr/bin/xenos-null.sh
install adv_swbutton /usr/bin/xenos-null.sh
install amd_hsmp /usr/bin/xenos-null.sh
install amd-pmc /usr/bin/xenos-null.sh
install amd-pmf /usr/bin/xenos-null.sh
install amilo-rfkill /usr/bin/xenos-null.sh
install apple-gmux /usr/bin/xenos-null.sh
install asus-laptop /usr/bin/xenos-null.sh
install asus-nb-wmi /usr/bin/xenos-null.sh
install asus-tf103c-dock /usr/bin/xenos-null.sh
install asus-wireless /usr/bin/xenos-null.sh
install asus-wmi /usr/bin/xenos-null.sh
install barco-p50-gpio /usr/bin/xenos-null.sh
install classmate-laptop /usr/bin/xenos-null.sh
install compal-laptop /usr/bin/xenos-null.sh
install alienware-wmi /usr/bin/xenos-null.sh
install dcdbas /usr/bin/xenos-null.sh
install dell-laptop /usr/bin/xenos-null.sh
install dell-rbtn /usr/bin/xenos-null.sh
install dell-smbios /usr/bin/xenos-null.sh
install dell-smo8800 /usr/bin/xenos-null.sh
install dell-wmi-aio /usr/bin/xenos-null.sh
install dell-wmi-ddv /usr/bin/xenos-null.sh
install dell-wmi-descriptor /usr/bin/xenos-null.sh
install dell-wmi-led /usr/bin/xenos-null.sh
install dell-wmi-sysman /usr/bin/xenos-null.sh
install dell-wmi /usr/bin/xenos-null.sh
install eeepc-laptop /usr/bin/xenos-null.sh
install eeepc-wmi /usr/bin/xenos-null.sh
install firmware_attributes_class /usr/bin/xenos-null.sh
install fujitsu-laptop /usr/bin/xenos-null.sh
install fujitsu-tablet /usr/bin/xenos-null.sh
install gigabyte-wmi /usr/bin/xenos-null.sh
install gpd-pocket-fan /usr/bin/xenos-null.sh
install hdaps /usr/bin/xenos-null.sh
install hp-bioscfg /usr/bin/xenos-null.sh
install hp-wmi /usr/bin/xenos-null.sh
install hp_accel /usr/bin/xenos-null.sh
install huawei-wmi /usr/bin/xenos-null.sh
install ibm_rtl /usr/bin/xenos-null.sh
install ideapad-laptop /usr/bin/xenos-null.sh
install intel_atomisp2_led /usr/bin/xenos-null.sh
install intel_atomisp2_pm /usr/bin/xenos-null.sh
install intel_ifs /usr/bin/xenos-null.sh
install intel_sar /usr/bin/xenos-null.sh
install intel_skl_int3472_discrete /usr/bin/xenos-null.sh
install intel_skl_int3472_tps68470 /usr/bin/xenos-null.sh
install intel-hid /usr/bin/xenos-null.sh
install intel-rst /usr/bin/xenos-null.sh
install intel-smartconnect /usr/bin/xenos-null.sh
install intel-vbtn /usr/bin/xenos-null.sh
install intel_bxtwc_tmu /usr/bin/xenos-null.sh
install intel_bytcrc_pwrsrc /usr/bin/xenos-null.sh
install intel_chtdc_ti_pwrbtn /usr/bin/xenos-null.sh
install intel_chtwc_int33fe /usr/bin/xenos-null.sh
install intel_crystal_cove_charger /usr/bin/xenos-null.sh
install intel_int0002_vgpio /usr/bin/xenos-null.sh
install intel_mrfld_pwrbtn /usr/bin/xenos-null.sh
install intel_oaktrail /usr/bin/xenos-null.sh
install intel_punit_ipc /usr/bin/xenos-null.sh
install intel_sdsi /usr/bin/xenos-null.sh
install intel_vsec /usr/bin/xenos-null.sh
install intel_vsec_tpmi /usr/bin/xenos-null.sh
install ishtp_eclite /usr/bin/xenos-null.sh
install pmt_class /usr/bin/xenos-null.sh
install pmt_crashlog /usr/bin/xenos-null.sh
install pmt_telemetry /usr/bin/xenos-null.sh
install isst_if_common /usr/bin/xenos-null.sh
install isst_if_mbox_msr /usr/bin/xenos-null.sh
install isst_if_mbox_pci /usr/bin/xenos-null.sh
install isst_if_mmio /usr/bin/xenos-null.sh
install isst_tpmi /usr/bin/xenos-null.sh
install isst_tpmi_core /usr/bin/xenos-null.sh
install intel_telemetry_core /usr/bin/xenos-null.sh
install intel_telemetry_debugfs /usr/bin/xenos-null.sh
install intel_telemetry_pltdrv /usr/bin/xenos-null.sh
install intel-uncore-frequency-common /usr/bin/xenos-null.sh
install intel-uncore-frequency-tpmi /usr/bin/xenos-null.sh
install intel-uncore-frequency /usr/bin/xenos-null.sh
install intel-wmi-sbl-fw-update /usr/bin/xenos-null.sh
install intel-wmi-thunderbolt /usr/bin/xenos-null.sh
install intel_ips /usr/bin/xenos-null.sh
install intel_scu_ipcutil /usr/bin/xenos-null.sh
install intel_scu_pltdrv /usr/bin/xenos-null.sh
install lenovo-ymc /usr/bin/xenos-null.sh
install lenovo-yogabook /usr/bin/xenos-null.sh
install lg-laptop /usr/bin/xenos-null.sh
install meraki-mx100 /usr/bin/xenos-null.sh
install mlx-platform /usr/bin/xenos-null.sh
install msi-ec /usr/bin/xenos-null.sh
install msi-laptop /usr/bin/xenos-null.sh
install msi-wmi /usr/bin/xenos-null.sh
install mxm-wmi /usr/bin/xenos-null.sh
install nvidia-wmi-ec-backlight /usr/bin/xenos-null.sh
install panasonic-laptop /usr/bin/xenos-null.sh
install pcengines-apuv2 /usr/bin/xenos-null.sh
install samsung-laptop /usr/bin/xenos-null.sh
install samsung-q10 /usr/bin/xenos-null.sh
install sel3350-platform /usr/bin/xenos-null.sh
install serial-multi-instantiate /usr/bin/xenos-null.sh
install simatic-ipc-batt-apollolake /usr/bin/xenos-null.sh
install simatic-ipc-batt-elkhartlake /usr/bin/xenos-null.sh
install simatic-ipc-batt-f7188x /usr/bin/xenos-null.sh
install simatic-ipc-batt /usr/bin/xenos-null.sh
install simatic-ipc /usr/bin/xenos-null.sh
install sony-laptop /usr/bin/xenos-null.sh
install system76_acpi /usr/bin/xenos-null.sh
install think-lmi /usr/bin/xenos-null.sh
install thinkpad_acpi /usr/bin/xenos-null.sh
install topstar-laptop /usr/bin/xenos-null.sh
install toshiba-wmi /usr/bin/xenos-null.sh
install toshiba_acpi /usr/bin/xenos-null.sh
install toshiba_bluetooth /usr/bin/xenos-null.sh
install toshiba_haps /usr/bin/xenos-null.sh
install winmate-fm07-keys /usr/bin/xenos-null.sh
install wireless-hotkey /usr/bin/xenos-null.sh
install wmi-bmof /usr/bin/xenos-null.sh
#install wmi /usr/bin/xenos-null.sh
install x86-android-tablets /usr/bin/xenos-null.sh
install xiaomi-wmi /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/drivers/acpi/
install platform_profile /usr/bin/xenos-null.sh
## Various Input Drivers
# /lib/modules/$(uname -r)/kernel/drivers/input/
install joydev /usr/bin/xenos-null.sh
install sparse-keymap /usr/bin/xenos-null.sh
## Various Webcam Drivers
# /lib/modules/$(uname -r)/kernel/drivers/media/usb/uvc/
install uvcvideo /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/drivers/media/common/
install uvc /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/drivers/media/common/videobuf2/
install videobuf2-common /usr/bin/xenos-null.sh
install videobuf2-dma-contig /usr/bin/xenos-null.sh
install videobuf2-dma-sg /usr/bin/xenos-null.sh
install videobuf2-dvb /usr/bin/xenos-null.sh
install videobuf2-memops /usr/bin/xenos-null.sh
install videobuf2-v4l2 /usr/bin/xenos-null.sh
install videobuf2-vmalloc /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/drivers/media/v4l2-core/
install tuner /usr/bin/xenos-null.sh
install v4l2-async /usr/bin/xenos-null.sh
install v4l2-cci /usr/bin/xenos-null.sh
install v4l2-dv-timings /usr/bin/xenos-null.sh
install v4l2-flash-led-class /usr/bin/xenos-null.sh
install v4l2-fwnode /usr/bin/xenos-null.sh
install v4l2-mem2mem /usr/bin/xenos-null.sh
install videodev /usr/bin/xenos-null.sh
# /lib/modules/$(uname -r)/kernel/drivers/media/mc/
install mc /usr/bin/xenos-null.sh

