#!/usr/bin/bash


#################################################
#
#           _______  _        _______  _______
# |\     /|(  ____ \( (    /|(  ___  )(  ____ \
# ( \   / )| (    \/|  \  ( || (   ) || (    \/
#  \ (_) / | (__    |   \ | || |   | || (_____
#   ) _ (  |  __)   | (\ \) || |   | |(_____  )
#  / ( ) \ | (      | | \   || |   | |      ) |
# ( /   \ )| (____/\| )  \  || (___) |/\____) |
# |/     \|(_______/|/    )_)(_______)\_______)
#
#
# This file is a part of the Xenos Install Kit.
# It adheres to the GNU GPL license.
#
# https://gitlab.com/xenos-install-kit/xenos-install-kit
#
# © 2020-2024
#
#
#################################################


initialize(){
  #
  ## Parameter variable prep
  #
  # -b | --use-basic-setup-only
  #      use_basic_setup_only: only sets up the active device and terminates
  # -l | --use-hourly-lease-termination creates:
  #      use_hourly_lease_termination: terminates connectivity automatically after 55 minutes
  #                                    (5 minutes before openvpn attempts to renew its lease automatically)
  # -s | --use-adv-security-checks creates:
  #      use_adv_security_checks: use advanced security checks
  #      linux_distribution_version: determines which distribution of linux is being used
  #      linux_firewall_version: the firewall version of the current distribution version of linux
  #
  if [[ -n $@ ]]; then
    for parameter in $@
    do
      case $parameter in
        -b | --use-basic-setup-only)
          use_basic_setup_only="y"
          ;;
        -l | --use-hourly-lease-termination)
          use_hourly_lease_termination="y"
          ;;
        -s | --use-adv-security-checks)
          use_adv_security_checks="y"
          linux_distribution_version=$(grep -i "pretty_name" /etc/os-release | sed "s/.*=//g" | sed "s/\"//g")

          case $linux_distribution_version in
            Linux* | Gentoo*)
              linux_firewall_version="iptables"
              ;;
            Ub*)
              linux_firewall_version="ufw"
              ;;
          esac
          ;;
      esac
    done
  fi

  #
  ## Device variable prep
  #
  # active_device_string: a string of device data used for manipulation
  #                       by default greps all connected devices then filters out special cases
  #                       eg "disconnected" often refers to wireless p2p devices
  #                          "connected (externally)" often refers to tap/tun devices
  # active_device_name: eg en* (ethernet) or wl* (wifi)
  # active_device_connection_name: eg the name of the connection ("Wired connection 1" "Wifi actually sucks")
  # active_device_domain: either init, ~. or empty
  #
  active_device_string=$(nmcli device | grep -i "connected" | grep -v -i "disconnected\|connected (externally)")
  active_device_name=$(echo "${active_device_string}" | awk '{print $1}' | sed "s/^[ \t]*//;s/[ \t]*$//" | sed "/^$/d")
  active_device_connection_name=$(echo "${active_device_string}" | awk '{$1=$2=$3=""; print $0}' | sed "s/^[ \t]*//;s/[ \t]*$//" | sed "/^$/d")
  active_device_domain="init"

  #
  ## Time variable prep
  #
  # activated_time: self explainatory
  # adjusted_time: the activated_time +55 minutes forward (5 minutes before openvpn attempts to renew its lease automatically)
  # SECONDS: bash shell/script time tracking variable
  #
  activated_time=$(date +"%I:%M")

  if [[ -n "${use_hourly_lease_termination}" ]]; then
    adjusted_time=$(date --date="-55 minutes ago" +"%I:%M")
    SECONDS=0
  fi

  #
  ## Loop variable prep
  #
  # connection_state: either init, 0, 1 or 2
  #                   init is a control state
  #                   0,1 are termination states
  #                   2 is a successful state
  #
  connection_state="init"

  return 0
}

generate_lease_message(){
  #
  ## Lease_message variable prep
  #
  # lease_message: determined entirely by the parameters used
  #
  if [[ -n "${use_basic_setup_only}" ]]; then
    lease_message="Xenos-control-dns.sh is running with the -b parameter and no openvpn lease is present."
  elif [[ -n "${use_hourly_lease_termination}" ]]; then
    lease_message="The current lease of the openvpn connection begins at ${activated_time} and expires at ${adjusted_time}."
  else
    lease_message="The current lease of the openvpn connection begins at ${activated_time} and will auto renew every hour."
  fi

  echo "Notice: ${lease_message}"

  return 0
}

update_vars(){
  #
  ## Connection_state=0 variables
  #
  # security_failure: various bugs that can result in data compromises
  # inactive_firewall: self explainatory
  #
  if [[ -n "${use_adv_security_checks}" ]]; then
    security_failure=$(journalctl | grep -i "failed to initiate ap scan\|no beacon heard and the time event is over already\|using degraded feature set" | grep -i -v "execve")
    inactive_firewall=$(systemctl status "${linux_firewall_version}" | grep -i "inactive")
  fi

  #
  ## Connection_state=1 variables
  #
  # active_device_connection_state: either disconnected or connected
  # active_device_domain: either init, ~. or empty
  # active_tunnel_name: eg tun*
  #
  active_device_connection_state=$(nmcli device | grep -i "${active_device_name}" | grep -v -i "p2p" | awk '{print $3}' | sed "s/^[ \t]*//;s/[ \t]*$//" | sed "/^$/d")

  if [[ "${active_device_domain}" == "init" || "${active_device_domain}" == "~." ]]; then
    active_device_domain=$(resolvectl domain "${active_device_name}" | awk '{print $4}')
  fi

  active_tunnel_name=$(nmcli device | grep -i "tun" | awk '{print $1}' | sed "s/^[ \t]*//;s/[ \t]*$//" | sed "/^$/d")

  return 0
}

check_connectivity_state(){
  # If the current session fails any of the security cases or the firewall is inactive then set connection_state=0
  if [[ -n "${use_adv_security_checks}" ]]; then
    if [[ -n "${security_failure}" || -n "${inactive_firewall}" ]]; then
      connection_message="A security case has failed."
      connection_state=0
      return 0
    fi
  fi

  # If the ethernet or wifi connection is inactive but the tun is active (i.e. device connection closed itself or was killed) then set connection_state=1
  if [[ "${active_device_connection_state}" == "disconnected" && -n "${active_tunnel_name}" ]]; then
    connection_message="The device connection closed itself or was killed."
    connection_state=1
    return 0
  fi

  # If the ethernet or wifi connection is active, its domain is empty and the tun is inactive (i.e. tunnel closed itself or was killed) then set connection_state=1
  if [[ "${active_device_connection_state}" == "connected"  && -z "${active_device_domain}" && -z "${active_tunnel_name}" ]]; then
    connection_message="The tunnel closed itself or was killed."
    connection_state=1
    return 0
  fi

  # If the current lease of the openvpn connection is about to expire then set connection_state=1
  if [[ -n "${use_hourly_lease_termination}" ]]; then
    # 3300 seconds = 55 minutes
    if (( $SECONDS >= 3300 )); then
      connection_message="The current lease of the openvpn connection is about to expire."
      connection_state=1
      return 0
    fi
  fi

  # If the ethernet or wifi connection is active, its domain isn't empty and the tun is active then set connection_state=2
  if [[ "${active_device_connection_state}" == "connected"  && -n "${active_device_domain}" && -n "${active_tunnel_name}" ]]; then
    connection_message="The current networking state has been setup successfully."
    connection_state=2
    return 0
  fi

  return 0
}

terminate_connectivity(){
  local xenos_device=$1

  if [[ -n "${xenos_device}" ]]; then
    nmcli device disconnect "${xenos_device}" &> /dev/null
  fi

  local leaky_applications=(
    # Torrent Clients
    "deluge-gtk"
    # VPN Clients
    "openvpn"
    # Web Browsers
    "brave"
    "chromium"
    "librewolf"
    "firefox"
    "luakit"
  )

  for application in "${leaky_applications[@]}"
  do
    local signal_type=""

    case $application in
      # Torrent Clients
      deluge-gtk)
        signal_type="-SIGINT"
        ;;
      # VPN Clients
      openvpn)
        signal_type="-SIGTERM"
        ;;
      # Web Browsers
      brave | chromium | librewolf | firefox)
        # SIGINT is needed for these web browsers so that the clear browsing data on exit feature is triggered
        # Additionally it prevents process lockups for librewolf and firefox
        signal_type="-SIGINT"
        ;;
      luakit)
        # SIGTERM is needed for these web browses as SIGINT is ignored
        signal_type="-SIGTERM"
        ;;
    esac

    pkill "${signal_type}" -f -i "${application}"
  done

  resolvectl reset-server-features
  resolvectl flush-caches
  resolvectl reset-statistics

  return 0
}

configure_connectivity_parameters(){
  local xenos_device=$1
  local xenos_connection=$2

  ip link set dev "${xenos_device}" allmulticast off
  ip link set dev "${xenos_device}" multicast off

  if [[ -n "${xenos_connection}" ]]; then
    if [[ "${xenos_device}" == wl* ]]; then
      nmcli connection mod "${xenos_connection}" 802-11-wireless.powersave 2
      nmcli connection mod "${xenos_connection}" 802-11-wireless.wake-on-wlan 0x8000

      local does_iw_exist=$(type -P iw)

      if [[ -n "${does_iw_exist}" ]]; then
        iw "${xenos_device}" set power_save off
      fi
    fi

    nmcli connection mod "${xenos_connection}" connection.lldp 0
    nmcli connection mod "${xenos_connection}" connection.llmnr 0
    nmcli connection mod "${xenos_connection}" connection.mdns 0
  fi

  resolvectl llmnr "${xenos_device}" 0
  resolvectl mdns "${xenos_device}" 0

  return 0
}

configure_connectivity_routing(){
  local xenos_device=$1
  local xenos_arg1=$2
  local xenos_arg2=$3

  resolvectl default-route "${xenos_device}" "${xenos_arg1}"
  resolvectl domain "${xenos_device}" "${xenos_arg2}"

  return 0
}

configure_connectivity_tunnel_name(){
  local ovpn_folder_path="/home/shared"
  local ovpn_folder_path_files=()

  if [[ -d "${ovpn_folder_path}" ]]; then
    ovpn_folder_path_files=("${ovpn_folder_path_files[@]}" $(find "${ovpn_folder_path}" -type f -iname "*.ovpn"))
  fi

  for file in "${ovpn_folder_path_files[@]}"
  do
    local tunnel_start="tun"
    local tunnel_mid=$(cat /dev/urandom | tr -dc "a-zA-Z0-9" | fold -w 8 | head -n 1)
    local tunnel_end=$(shuf -i 10-40 -n 1)
    local tunnel_name="${tunnel_start}-${tunnel_mid}-${tunnel_end}"

    sed -i "s|^dev ${tunnel_start}.*|dev ${tunnel_name}|g" "${file}"
  done

  return 0
}


## Initialize script
if (( $EUID != 0 )); then
  exit 0
fi

initialize $@

if [[ -n "${active_device_name}" ]]; then
  generate_lease_message
  configure_connectivity_parameters "${active_device_name}" "${active_device_connection_name}"
  configure_connectivity_routing "${active_device_name}" "1" "~."
  configure_connectivity_tunnel_name

  if [[ -n "${use_basic_setup_only}" ]]; then
    echo "Success: The active device has been setup successfully. Now terminating."
  else
    while :
    do
      update_vars
      check_connectivity_state

      if [[ "${connection_state}" != "init" ]]; then
        case $connection_state in
          0 | 1)
            echo "Warning: ${connection_message} Now terminating the current networking state."
            terminate_connectivity "${active_device_name}"
            break
            ;;
          2)
            echo "Success: ${connection_message}"
            configure_connectivity_parameters "${active_tunnel_name}"
            configure_connectivity_routing "${active_device_name}" "0" ""
            configure_connectivity_routing "${active_tunnel_name}" "1" "~."
            connection_state="init"
            ;;
        esac
      fi

      sleep 3
    done
  fi
fi

exit 0

