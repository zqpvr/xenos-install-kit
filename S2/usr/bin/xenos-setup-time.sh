#!/usr/bin/bash


#################################################
#
#           _______  _        _______  _______
# |\     /|(  ____ \( (    /|(  ___  )(  ____ \
# ( \   / )| (    \/|  \  ( || (   ) || (    \/
#  \ (_) / | (__    |   \ | || |   | || (_____
#   ) _ (  |  __)   | (\ \) || |   | |(_____  )
#  / ( ) \ | (      | | \   || |   | |      ) |
# ( /   \ )| (____/\| )  \  || (___) |/\____) |
# |/     \|(_______/|/    )_)(_______)\_______)
#
#
# This file is a part of the Xenos Install Kit.
# It adheres to the GNU GPL license.
#
# https://gitlab.com/xenos-install-kit/xenos-install-kit
#
# © 2020-2024
#
#
#################################################


initialize(){
  active_device_string=$(nmcli device | grep -i "connected" | grep -v -i "disconnected\|connected (externally)")
  active_device_name=$(echo "${active_device_string}" | awk '{print $1}' | sed "s/^[ \t]*//;s/[ \t]*$//" | sed "/^$/d")

  return 0
}

fetch_time(){
  local pools=(
    "https://archlinux.org"
    "https://freenet.org"
    "https://matrix.org"
    "https://openvpn.net/community"
    "https://tails.boum.org"
    "https://www.coreboot.org"
    "https://www.gentoo.org"
    "https://www.linuxfromscratch.org"
    "https://www.qubes-os.org"
    "https://www.torproject.org"
    "https://www.whonix.org"
  )

  local secure_curl="curl -sI --tlsv1.3 --proto =https -m 10"
  selected_pool=$(shuf -n 1 -e "${pools[@]}")
  new_time=$( ${secure_curl} ${selected_pool} | grep -i "date" | grep -v -i "revalidate" | sed "s/date: //g" | sed "s/Date: //g")

  return 0
}

setup_time(){
  local secure_time=$1

  date -s "${secure_time}" -u &> /dev/null
  hwclock -w -u

  return 0
}


## Initialize script
if (( $EUID != 0 )); then
  exit 0
fi

initialize

if [[ -n "${active_device_name}" ]]; then
  fetch_time

  while :
  do
    if [[ -n "${new_time}" ]]; then
      echo "Success: ${selected_pool} supports all supplied options and is reachable. The system time will now be set."
      setup_time "${new_time}"
      break
    else
      echo "Warning: ${selected_pool} doesn't support all supplied options or is unreachable."
      echo "Notice: Now selecting a different pool."
      fetch_time
    fi

    sleep 1
  done
fi

exit 0

