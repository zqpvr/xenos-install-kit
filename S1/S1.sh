#!/usr/bin/bash


#################################################
#
#           _______  _        _______  _______
# |\     /|(  ____ \( (    /|(  ___  )(  ____ \
# ( \   / )| (    \/|  \  ( || (   ) || (    \/
#  \ (_) / | (__    |   \ | || |   | || (_____
#   ) _ (  |  __)   | (\ \) || |   | |(_____  )
#  / ( ) \ | (      | | \   || |   | |      ) |
# ( /   \ )| (____/\| )  \  || (___) |/\____) |
# |/     \|(_______/|/    )_)(_______)\_______)
#
#
# This file is a part of the Xenos Install Kit.
# It adheres to the GNU GPL license.
#
# https://gitlab.com/xenos-install-kit/xenos-install-kit
#
# © 2020-2024
#
#
#################################################


# Print commands before executing and exit when any command fails
set -xe


initialize(){
  #
  ## Variable prep
  #
  keymap_language_split="en_US.UTF-8"
  keymap=$(echo "${keymap_language_split}" | sed "s/.*_//g" | sed "s/\..*//g" | tr A-Z a-z)
  drive_name="/dev/sda"
  luks_password=''
  luks_container_name="luks"
  luks_volume_name=$(cat /dev/urandom | tr -dc "a-z" | fold -w 8 | head -n 1)
  luks_volume_fs_type="xfs"
  kernel_type="linux-hardened"
  mirrorlist_url="https://archlinux.org/mirrorlist/?country=IS&country=NO&country=CH&protocol=https&ip_version=4&use_mirror_status=on"
  core_pack="apparmor axel base base-devel dbus-daemon-units dhcpcd git ${kernel_type} ${kernel_type}-headers ${kernel_type}-docs linux-firmware lvm2 mkinitcpio mg"
  case $luks_volume_fs_type in
    btrfs)
      core_pack="${core_pack} btrfs-progs"
      ;;
    ext4)
      core_pack="${core_pack} e2fsprogs"
      ;;
    xfs)
      core_pack="${core_pack} xfsprogs"
      ;;
  esac
  is_intel_cpu=$(lscpu | grep -i "intel(r)" 2> /dev/null || echo "")
  if [[ -n "${is_intel_cpu}" ]]; then
    cpu_type="intel"
  else
    cpu_type="amd"
  fi
  core_pack="${core_pack} ${cpu_type}-ucode"
  systemd_git_url=""
  dns_servers="9.9.9.9 142.112.112.112"
  timezone="America/New_York"
  language="${keymap_language_split}"
  hostname='host'
  username='user'
  userpass=''
  using_usb_keyboard=""
  if [[ -n "${using_usb_keyboard}" ]]; then
    mkinitcpio_hooks="base keyboard keymap udev autodetect modconf kms consolefont block encrypt lvm2 filesystems fsck"
  else
    mkinitcpio_hooks="base udev autodetect modconf kms keyboard keymap consolefont block encrypt lvm2 filesystems fsck"
  fi
  systemdboot_entry_name=$(cat /dev/urandom | tr -dc "a-zA-Z0-9" | fold -w 16 | head -n 1)
  systemdboot_entry_name="${systemdboot_entry_name}.conf"
  systemdboot_entry_content="title Linux\nlinux /vmlinuz-${kernel_type}\ninitrd /${cpu_type}-ucode.img\ninitrd /initramfs-${kernel_type}.img\noptions"
  #
  ## Begin systemdboot_entry_content_options generation
  # systemdboot_entry_content_options (1/2)
  #
  # Core security features
  systemdboot_entry_content_options="apparmor=1 lsm=landlock,lockdown,yama,integrity,apparmor,bpf"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} lockdown=confidentiality"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} module.sig_enforce=1"
  # CPU mitigations
  systemdboot_entry_content_options="${systemdboot_entry_content_options} gather_data_sampling=force"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} kvm.nx_huge_pages=force"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} l1d_flush=on"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} l1tf=full,force"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} mds=full,nosmt"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} mmio_stale_data=full,nosmt"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} nosmt=force"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} retbleed=auto,nosmt"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} spectre_v2=on"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} spec_rstack_overflow=safe-ret"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} spec_store_bypass_disable=on"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} tsx=off tsx_async_abort=full,nosmt"
  # DMA hardening
  systemdboot_entry_content_options="${systemdboot_entry_content_options} efi=disable_early_pci_dma"
  if [[ -n "${is_intel_cpu}" ]]; then
    systemdboot_entry_content_options="${systemdboot_entry_content_options} intel_iommu=on"
  else
    systemdboot_entry_content_options="${systemdboot_entry_content_options} amd_iommu=force_isolation"
  fi
  systemdboot_entry_content_options="${systemdboot_entry_content_options} iommu=force iommu.passthrough=0 iommu.strict=1"
  # Entropy hardening
  systemdboot_entry_content_options="${systemdboot_entry_content_options} extra_latent_entropy"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} random.trust_bootloader=off"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} random.trust_cpu=off"
  # Kernel hardening
  systemdboot_entry_content_options="${systemdboot_entry_content_options} init_on_alloc=1 init_on_free=1"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} mce=off"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} oops=panic"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} page_alloc.shuffle=1"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} pti=on"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} randomize_kstack_offset=1"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} slab_nomerge"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} vsyscall=none"
  # Misc vendor fixes
  if [[ -n "${is_intel_cpu}" ]]; then
    systemdboot_entry_content_options="${systemdboot_entry_content_options} intel_pstate=hwp_only modprobe.blacklist=nouveau"
  fi
  systemdboot_entry_content_options="${systemdboot_entry_content_options} pci=noaer"
  # Userspace hardening
  systemdboot_entry_content_options="${systemdboot_entry_content_options} biosdevname=0 net.ifnames=0"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} debugfs=off"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} hostname=${hostname}"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} ipv6.disable=1"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} nohibernate"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} nowatchdog"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} systemd.crash_chvt=no systemd.crash_shell=no"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} systemd.dump_core=no coredump_filter=0x0"

  return 0
}

presetup(){
  # The following services need stopped early on because they create race conditions
  # which often terminates the installation process
  local problematic_services=(
    "pacman-init.service"
    "reflector.service"
  )

  for service in "${problematic_services[@]}"
  do
    systemctl stop "${service}"
  done

  # Set the keymap
  localectl set-keymap "${keymap}"
  loadkeys "${keymap}"

  # Set the system time
  timedatectl set-ntp true

  return 0
}

setup_drive(){
  ## Wipe the drive
  # Remove all magic strings
  local existing_partitions=($(ls -A -r /dev | grep -i "${drive_name##*/}"))

  for partition in "${existing_partitions[@]}"
  do
    wipefs -a -f "/dev/${partition}"
  done

  # Zap (destroy) the GPT and MBR data structures
  sgdisk -Z "${drive_name}"

  # Generate new GPT headers on the drive
  sgdisk -a 2048 -o "${drive_name}"

  ###########################################################
  # Partition      Size          Type                  Code #
  #---------------------------------------------------------#
  # /dev/sdx1      1G            EFI                   ef00 #
  # /dev/sdx2      100%FREE      Linux LUKS            8300 #
  ###########################################################

  # Create the drive partitions
  sgdisk -n 1:0:+1G "${drive_name}"
  sgdisk -n 2:0:0 "${drive_name}"

  # Set the drive partition types
  sgdisk -t 1:ef00 "${drive_name}"
  sgdisk -t 2:8300 "${drive_name}"

  # Label the drive partitions
  sgdisk -c 1:"EFI" "${drive_name}"
  sgdisk -c 2:"Linux LUKS" "${drive_name}"

  # Format the drive partitions
  yes | mkfs.fat -F 32 "${drive_name}1"

  printf '%s' "$luks_password" | cryptsetup luksFormat --type luks2 --pbkdf argon2id -h sha512 -s 512 -i 20000 "${drive_name}2"
  printf '%s' "$luks_password" | cryptsetup open "${drive_name}2" "${luks_container_name}"
  yes | pvcreate "/dev/mapper/${luks_container_name}"
  yes | vgcreate "${luks_volume_name}" "/dev/mapper/${luks_container_name}"
  yes | lvcreate -l 100%FREE "${luks_volume_name}" -n root
  yes | mkfs."${luks_volume_fs_type}" "/dev/${luks_volume_name}/root"

  # Refresh drive partition table changes
  partprobe

  #
  ## Finish systemdboot_entry_content_options generation now that the luks UUID is available
  # systemdboot_entry_content_options (2/2)
  #
  luks_uuid=$(blkid -s UUID -o value "${drive_name}2")
  systemdboot_entry_content_options="${systemdboot_entry_content_options} cryptdevice=UUID=${luks_uuid}:${luks_container_name} root=/dev/${luks_volume_name}/root"
  systemdboot_entry_content_options="${systemdboot_entry_content_options} quiet loglevel=0 systemd.show_status=no udev.log_level=0 vt.global_cursor_default=0 fbcon=font:TER16x32 rw"
  systemdboot_entry_content="${systemdboot_entry_content} ${systemdboot_entry_content_options}"

  # Mount the drive partitions
  mount "/dev/${luks_volume_name}/root" /mnt
  mkdir -p /mnt/boot
  mount "${drive_name}1" /mnt/boot

  return 0
}

setup_system(){
  # Regenerate pacman mirrorlist
  curl -o mirrorlist "${mirrorlist_url}" && sleep 3

  if [[ -f "mirrorlist" && -s "mirrorlist" ]]; then
    sed -i "s/^#Server/Server/g" mirrorlist
    cp mirrorlist /etc/pacman.d/
  else
    echo "Warning: The mirrorlist_url wasn't reachable. Now exiting..."
    exit 0
  fi

  # Update pacman databases and force archlinux-keyring refresh
  pacman -Syy && pacman -S --noconfirm archlinux-keyring

  # Begin pacstrap
  pacstrap -K /mnt $core_pack

  # Generate fstab
  genfstab -U /mnt >> /mnt/etc/fstab

  # Setup keymap files
  mkdir -p "/mnt/etc/X11/xorg.conf.d/"
  cp /etc/X11/xorg.conf.d/00-keyboard.conf /mnt/etc/X11/xorg.conf.d/01_keyboard_configuration.conf
  cp /etc/vconsole.conf /mnt/etc/

  # Setup pacman mirrorlist
  cp /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/

  # Setup systemd-git (1/2)
  if [[ -n "${systemd_git_url}" ]]; then
    curl -o systemd-git.tar.xz "${systemd_git_url}" && sleep 3

    if [[ -f "systemd-git.tar.xz" && -s "systemd-git.tar.xz" ]]; then
      tar -xaf systemd-git.tar.xz
      mkdir -p "/mnt/home/systemd-tmp/"
      cp -R systemd-git/ /mnt/home/systemd-tmp/
    else
      echo "Warning: The systemd_git_url wasn't reachable. Now exiting..."
      exit 0
    fi
  fi

  return 0
}

enter_chroot(){
arch-chroot /mnt /usr/bin/bash <<EOF
  # Setup systemd-git (2/2)
  if [[ -n "${systemd_git_url}" ]]; then
    if [[ -d "/home/systemd-tmp/systemd-git" ]]; then
      cd /home/systemd-tmp/systemd-git
      yes | pacman -U *
      cd /
      rm -rf /home/systemd-tmp
    fi
  fi

  # Setup dhcpcd
  echo -e "# Use custom DNS\nstatic domain_name_servers=${dns_servers}" | tee -a /etc/dhcpcd.conf > /dev/null
  systemctl enable dhcpcd.service

  # Set the timezone
  ln -sf "/usr/share/zoneinfo/${timezone}" /etc/localtime
  hwclock -w -u

  # Set the locale
  sed -i "s/^#${language}/${language}/g" /etc/locale.gen
  locale-gen
  echo "LANG=${language}" > /etc/locale.conf

  # Configure the networking
  echo "${hostname}" > /etc/hostname
  echo -e "127.0.0.1 localhost\n127.0.1.1 ${hostname}" > /etc/hosts

  # Configure sudo
  sed -i "s/^# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/g" /etc/sudoers

  # Create our user
  useradd -m -g users -G wheel -s /usr/bin/bash "$username"
  printf '%s' "$username:$userpass" | chpasswd

  # Configure mkinitcpio for luks
  sed -i "s/^HOOKS=.*/HOOKS=(${mkinitcpio_hooks})/g" /etc/mkinitcpio.conf

  # Configure systemd-boot
  bootctl install
  sed -i "d" /boot/loader/loader.conf
  echo -e "default ${systemdboot_entry_name}\ntimeout 0\nconsole-mode max\neditor 0" > /boot/loader/loader.conf
  echo -e "${systemdboot_entry_content}" > "/boot/loader/entries/${systemdboot_entry_name}"

  # Regenerate mkinitcpio
  mkinitcpio -P

  # Exit chroot
  exit
EOF
}

exit_installer(){
  # Dismount any mounted partitions
  umount -A --recursive /mnt

  # Prompt for shutdown
  read -p "Xenos base install complete. Press [Enter] key to shutdown..."
  systemctl poweroff

  return 0
}


initialize
presetup
setup_drive
setup_system
enter_chroot
exit_installer

exit 0

